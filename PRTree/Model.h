#pragma once

#include <vector>
#include "ModelData.h"
#include "Shape.h"

class Model
{
protected:
  std::shared_ptr<ModelData> _data;
public:
	Model(const std::shared_ptr<ModelLoader>& loader, const std::string& source);
  ModelData& GetModelData() { return *_data; }
  const ModelData& GetModelData() const { return *_data; }
	virtual ~Model();
};

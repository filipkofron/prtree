#pragma once

class TextureD3D11;

#include "D3D11Dev.h"
#include <string>

class TextureD3D11
{
public:
  TextureD3D11(D3D11Dev& dev, const std::string& filename);
  ~TextureD3D11();
  ID3D11ShaderResourceView* GetTextureResourceView() const { return _textureResourceView.Get(); }
  ID3D11Resource* GetTexture() const { return _texture.Get(); }
  const std::string& GetSource() const { return _source; }
  std::string& GetSource() { return _source; }
  void Apply(ID3D11DeviceContext& devContext);
protected:
  ComPtr<ID3D11ShaderResourceView> _textureResourceView;
  ComPtr<ID3D11Resource> _texture;
  std::string _source;
};

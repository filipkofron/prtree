#include "StringUtils.h"
#include <stdlib.h>

namespace StringUtils
{
  std::string WideStringToMultibyte(const std::wstring& wideStr)
  {
    // 8 times the wide string length should be enough
    size_t len = wideStr.size();
    size_t maxLen = len * 8;
    size_t maxBuf = maxLen + 1;
    std::string res;
    res.resize(maxBuf);
    size_t converted = 0;
    if (wcstombs_s(&converted, &res[0], maxLen, wideStr.data(), maxLen) != 0)
      ExitWith("Error converting wide string to multibyte!");
    res[converted] = '\0';

    return res;
  }

  std::wstring MultibyteStringToWide(const std::string& wideStr)
  {
    // 8 times the wide string length should be enough
    size_t len = wideStr.size();
    size_t maxLen = len * 8;
    size_t maxBuf = maxLen + 1;
    std::wstring res;
    res.resize(maxBuf);
    size_t converted = 0;
    if (mbstowcs_s(&converted, &res[0], maxLen, wideStr.data(), maxLen) != 0)
      ExitWith("Error converting multibyte string to wide!");
    res[converted] = '\0';
    return res;
  }
}
#include "SHTex.hlsli"

#define MATERIALS_MAX 32

Texture2DArray _ambientTextures : register(t0);
Texture2DArray _diffuseTextures : register(t1);
Texture2DArray _specularTextures : register(t2);
Texture2DArray _specularHighlightsTextures : register(t3);
Texture2DArray _bumpTextures : register(t4);
Texture2DArray _displacementTextures : register(t5);
Texture2DArray _alphaTextures : register(t6);
SamplerState linearSampler;
/*
struct MaterialType
{
float3 ambient;
float3 diffuse;
float3 specular;
float3 transmittance;
float3 emission;
float shininess;
float dissolve;
int ambientTextureIdx;
int diffuseTextureIdx;
int specularTextureIdx;
int specularHighlightTextureIdx;
int bumpTextureIdx;
int displacementTextureIdx;
};

cbuffer MaterialLibrary : register(b1)
{
MaterialType materials[16];
}*/

cbuffer MaterialLibrary : register(b1)
{
  float4 ambient[MATERIALS_MAX];
  float4 diffuse[MATERIALS_MAX];
  float4 specular[MATERIALS_MAX];
  float4 transmittance[MATERIALS_MAX];
  float4 emission[MATERIALS_MAX];
  float4 sdad[MATERIALS_MAX]; // [shininess coef, dissolve coef, ambient tex id, diffuse tex id]
  float4 ssbd[MATERIALS_MAX]; // [specular tex id, specural high tex id, bump tex id, displacement tex id]
  float4 flags[MATERIALS_MAX]; // [is leaf, none, none, none]
}

PixelOutputType main(PixelInputType input)
{
  PixelOutputType result;
  result.outputColor.rgba = float4(input.shLight, 1.0);
  result.outputColor.rgb *= 4;
  result.depth = input.realPos.z / input.realPos.w;
  return result;
}

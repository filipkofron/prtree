#pragma once

#include "Float3.h"
#include <cmath>

class Light
{
public:
  enum class Type
  {
    Directional, // Sun
    HalfSphere // Ambient simulation
  };

  Light(Type type, Float3 position, Float3 direction, Float3 color)
    : _type(type), _position(position), _direction(direction), _color(color)
  {

  }

  //! Color from direction. Note: must use normalized directions.
  Float3 EvalAtDir(Float3 dir)
  {
    switch (_type)
    {
    case Type::Directional:
    {
      float lightCoef = fmin(fmax(Dot(dir, _direction), 0.0f), 1.0f);
      return _color * lightCoef;
    }
    case Type::HalfSphere:
    {
      float lightCoef = fmin(fmax(Dot(dir, _direction), 0.0f), 1.0f) > 0 ? 1.0f : 0.0f;
      return _color * lightCoef;
    }
    default:
      return Float3(0.0f, 0.0f, 0.0f);
    }
  }

private:
  Float3 _position;
  Float3 _direction;
  Float3 _color;
  Type _type;
};
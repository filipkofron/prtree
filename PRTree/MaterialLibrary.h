#pragma once
#include <iostream>

class MaterialLibrary
{
public:
  MaterialLibrary() { }
  virtual ~MaterialLibrary() { }

  virtual bool DeserializeNative(std::istream& input) = 0;
  virtual void SerializeNative(std::ostream& output) = 0;
};

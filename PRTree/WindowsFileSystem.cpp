#include "WindowsFileSystem.h"
#include "System.h"
#include <Shlwapi.h>
#include <Shlobj.h>

#pragma comment(lib, "Shlwapi.lib")

void WindowsFileSystem::CreateDirectoryPath(const std::string& path)
{
  char cwd[MAX_PATH];
  GetCurrentDirectory(MAX_PATH, cwd);
  std::string absPath = cwd;
  absPath += "/";
  absPath += path;
  size_t bufSize = absPath.size() + 1;
  char* temp = new char[bufSize];
  strcpy_s(temp, bufSize, absPath.c_str());
  char* tempP = temp;
  while (*tempP)
  {
    *tempP = *tempP == '/' ? '\\' : *tempP;
    tempP++;
  }
  PathRemoveFileSpec(temp);
  auto ret = SHCreateDirectoryEx(NULL, temp, NULL);
  if (ret != ERROR_ALREADY_EXISTS && ret != ERROR_SUCCESS)
    ExitWith("Could not create path: '" << temp << "'!!");
  delete[] temp;
}

std::string WindowsFileSystem::GetWorkingDirectory()
{
  char cwd[MAX_PATH];
  GetCurrentDirectory(MAX_PATH, cwd);
  std::string absPath = cwd;
  size_t bufSize = absPath.size() + 1;
  char* temp = new char[bufSize];
  strcpy_s(temp, bufSize, absPath.c_str());
  char* tempP = temp;
  while (*tempP)
  {
    *tempP = *tempP == '/' ? '\\' : *tempP;
    tempP++;
  }
  std::string ret = temp;
  delete[] temp;
  return ret;
}

std::string WindowsFileSystem::GetDirectoryPath(const std::string& path)
{
  size_t bufSize = path.size() + 1;
  char* temp = new char[bufSize];
  strcpy_s(temp, bufSize, path.c_str());
  char* tempP = temp;
  while (*tempP)
  {
    *tempP = *tempP == '/' ? '\\' : *tempP;
    tempP++;
  }
  PathRemoveFileSpec(temp);
  std::string ret = temp;
  delete[] temp;
  return ret + "\\";
}

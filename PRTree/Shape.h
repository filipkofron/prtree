#pragma once
#include <iostream>
#include <vector>

class Shape
{
public:
  Shape() { }
  virtual ~Shape() { }

  virtual bool DeserializeNative(std::istream& input) = 0;
  virtual void SerializeNative(std::ostream& output) = 0;

  virtual size_t GetVerticesCount() = 0;
  virtual void GetVertices(std::vector<float>& dest) = 0;

  virtual size_t GetIndexesCount() = 0;
  virtual void GetIndexes(std::vector<int>& dest) = 0;
};

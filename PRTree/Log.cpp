#include "Log.h"
#include <iostream>

Logger Error(LogType::LOG_ERROR);
Logger Warn(LogType::LOG_WARN);
Logger Info(LogType::LOG_INFO);
Logger Debug(LogType::LOG_DEBUG);

#define LOG_TYPE_TO_STRING(type) case LogType::type: return #type;

std::string LogTypeToString(const LogType& logType)
{
  switch (logType)
  {
    LOG_TYPES(LOG_TYPE_TO_STRING)
  }

  return "Unknown";
}

Logger::Logger(LogType logType)
  : _logType(logType)
{

}

Logger::~Logger()
{
}

void Logger::Print(const std::string& str)
{
  for (auto& receiver : _logReceivers)
  {
    receiver->OnMessage(str, _logType);
  }
}

std::mutex Logger::_logLock;
std::vector<std::shared_ptr<LogReceiver> > Logger::_logReceivers;

void Logger::AddLogReceiver(const std::shared_ptr<LogReceiver>& logReceiver)
{
  std::lock_guard<std::mutex> guard(_logLock);
  _logReceivers.push_back(logReceiver);
}

void Logger::RemoveLogReceiver(const std::shared_ptr<LogReceiver>& logReceiver)
{
  std::lock_guard<std::mutex> guard(_logLock);
  for (std::vector<std::shared_ptr<LogReceiver> >::iterator it = _logReceivers.begin(); it != _logReceivers.end(); it++)
  {
    if (*it == logReceiver)
    {
      it = _logReceivers.erase(it);
      if (it == _logReceivers.end())
        break;
    }
  }
}

#define LOG_TYPE_OPERATOR_DEFINE(type) \
Logger& operator << (Logger& logger, type typeValue) \
{ \
  std::lock_guard<std::mutex> guard(logger._logLock); \
  logger._ss.str(""); \
  logger._ss << typeValue; \
\
  logger.Print(logger._ss.str()); \
  return logger; \
}

DEFINE_LOG_TYPE(LOG_TYPE_OPERATOR_DEFINE)

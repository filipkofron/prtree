#include "PRTBuilder.h"
#include "PRTSampler.h"
#include "PRTEngine.h"

void PRTBuilder::Build(std::vector<float>& coeffs)
{
  static const int samplerNum = 128;
  static const int samplesNum = 32;
  std::vector<PRTSampler> samplers(samplerNum);
  for (int i = 0; i < samplerNum; i++)
  {
    samplers[i].ComputeSamples(samplesNum);
  }

  static const int Bands = 4; // hardcoded throughout the code

  PRTEngine engine;
  engine.ComputeSH(samplers, Bands);
  std::vector<std::vector<PRTVector3> > prtCoeffs;

  engine.ProjectShadowed(prtCoeffs, samplers, _model, Bands);

  coeffs.resize(prtCoeffs.size() * Bands * Bands);
  for (int i = 0; i < prtCoeffs.size(); i++)
  {
    // merge albedo colors that are not used
    for (int j = 0; j < Bands * Bands; j++)
    {
      float val = prtCoeffs[i][j]._x + prtCoeffs[i][j]._y + prtCoeffs[i][j]._z;
      coeffs[i * Bands * Bands + j] = val / 3.0f;
    }
  }
}

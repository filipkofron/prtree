#include "System.h"
#include "ShapeD3D11.h"

ShapeD3D11::ShapeD3D11(D3D11Dev& dev)
  : _d3d11Dev(dev)
{

}

ShapeD3D11::~ShapeD3D11()
{
}

void ShapeD3D11::Render(ID3D11DeviceContext& dev)
{
  RenderBuffers(dev);
  dev.DrawIndexed(static_cast<UINT>(_indexCount), 0, 0);
}

bool ShapeD3D11::DeserializeNative(std::istream& input)
{
  D3D11_BUFFER_DESC vertexBufferDesc, indexBufferDesc;
  D3D11_SUBRESOURCE_DATA vertexData, indexData;
  HRESULT result;

  size_t vertexBufferBytes = 0;
  input.read(reinterpret_cast<char*>(&vertexBufferBytes), sizeof(vertexBufferBytes));

  if (input.fail())
    ExitWith("Fail while reading native model!");

  uint8_t* pureData = new uint8_t[vertexBufferBytes];
  input.read(reinterpret_cast<char*>(pureData), vertexBufferBytes);

  if (input.fail())
    ExitWith("Fail while reading native model!");

  VertexType* data = reinterpret_cast<VertexType*>(pureData);

  vertexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
  vertexBufferDesc.ByteWidth = static_cast<UINT>(vertexBufferBytes);
  vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
  vertexBufferDesc.CPUAccessFlags = 0;
  vertexBufferDesc.MiscFlags = 0;
  vertexBufferDesc.StructureByteStride = 0;

  // Give the subresource structure a pointer to the vertex data.
  vertexData.pSysMem = pureData;
  vertexData.SysMemPitch = 0;
  vertexData.SysMemSlicePitch = 0;

  // Now create the vertex buffer.
  result = _d3d11Dev.GetDevice().CreateBuffer(&vertexBufferDesc, &vertexData, _vertexBuffer.GetAddressOf());
  if (FAILED(result))
    ExitWith("Could not create vertex buffer!");
  
  delete[] pureData;

  size_t indexBufferBytes;
  input.read(reinterpret_cast<char*>(&indexBufferBytes), sizeof(indexBufferBytes));

  if (input.fail())
    ExitWith("Fail while reading native model!");

  pureData = new uint8_t[indexBufferBytes];
  input.read(reinterpret_cast<char*>(pureData), indexBufferBytes);

  int* iData = reinterpret_cast<int*>(pureData);

  // Set up the description of the static index buffer.
  indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
  indexBufferDesc.ByteWidth = static_cast<UINT>(indexBufferBytes);
  indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
  indexBufferDesc.CPUAccessFlags = 0;
  indexBufferDesc.MiscFlags = 0;
  indexBufferDesc.StructureByteStride = 0;

  // Give the subresource structure a pointer to the index data.
  indexData.pSysMem = pureData;
  indexData.SysMemPitch = 0;
  indexData.SysMemSlicePitch = 0;

  // Create the index buffer.
  result = _d3d11Dev.GetDevice().CreateBuffer(&indexBufferDesc, &indexData, _indexBuffer.GetAddressOf());
  if (FAILED(result))
    ExitWith("Could not create index buffer!");

  delete[] pureData;
  _vertexCount = vertexBufferBytes / sizeof(VertexType);
  _indexCount = indexBufferBytes / sizeof(UINT);

  return true;
}

void ShapeD3D11::SerializeNative(std::ostream& output)
{
  _d3d11Dev.SerializeBuffer(output, *_vertexBuffer.Get());
  _d3d11Dev.SerializeBuffer(output, *_indexBuffer.Get());
}

size_t ShapeD3D11::GetVerticesCount()
{
  return _vertexCount;
}

void ShapeD3D11::GetVertices(std::vector<float>& dest)
{
  return _d3d11Dev.ReadBufferToRaw(*_vertexBuffer.Get(), dest);
}

size_t ShapeD3D11::GetIndexesCount()
{
  return _indexCount;
}

void ShapeD3D11::GetIndexes(std::vector<int>& dest)
{
  return _d3d11Dev.ReadBufferToRaw(*_indexBuffer.Get(), dest);
}

void ShapeD3D11::RenderBuffers(ID3D11DeviceContext& devContext)
{
  unsigned int stride;
  unsigned int offset;

  // Set vertex buffer stride and offset.
  stride = sizeof(VertexType);
  offset = 0;

  // Set the vertex buffer to active in the input assembler so it can be rendered.
  devContext.IASetVertexBuffers(0, 1, _vertexBuffer.GetAddressOf(), &stride, &offset);

  // Set the material buffer
  devContext.PSSetConstantBuffers(1, 1, _materialLibrary->GetMaterialBuffer().GetAddressOf());

  // Set textures
  _materialLibrary->Apply(devContext);

  // Set the index buffer to active in the input assembler so it can be rendered.
  devContext.IASetIndexBuffer(_indexBuffer.Get(), DXGI_FORMAT_R32_UINT, 0);

  // Set the type of primitive that should be rendered from this vertex buffer, in this case triangles.
  devContext.IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
}

#pragma once

#include "WindowsSystem.h"
#include "WindowsGraphics.h"
#include "WindowsInput.h"
#include "WindowsPreciseTimer.h"
#include "WindowsLog.h"
#include "Log.h"
#include "Version.h"
#include "Config.h"

#include <memory>

#define GWindowsSystem (static_cast<WindowsSystem*>(GSystem))

WindowsSystem::WindowsSystem()
{
  RedirectIOToConsole();
}

WindowsSystem::~WindowsSystem()
{

}

void WindowsSystem::debug_Sleep(uint64_t millis)
{
  Sleep(static_cast<DWORD>(millis));
}

std::shared_ptr<LogReceiver> WindowsSystem::CreateDefaultLogReceiver()
{
  return ::CreateDefaultLogReceiver();
}

void WindowsSystem::ExitWithError(std::function<void(std::ostream&)> msgFun)
{
  std::stringstream ss;
  msgFun(ss);
  Error << ss.str() << "\n";
  std::string str = ss.str();
  MessageBox(_windowHandle, str.c_str(), "Error", MB_OK);
  __debugbreak();
  //exit(1);
}

float WindowsSystem::GetTimeFloat()
{
  using FpMilliseconds =
    std::chrono::duration<float, std::chrono::milliseconds::period>;
  static_assert(std::chrono::treat_as_floating_point<FpMilliseconds::rep>::value,
    "Rep required to be floating point");
  auto now = std::chrono::high_resolution_clock::now();
  return FpMilliseconds(now - _startTime).count();
}

void WindowsSystem::OnInit()
{
  _startTime = std::chrono::high_resolution_clock::now();

  SetLowPriority();
  SetupWindows();
}

void WindowsSystem::OnShutdown()
{
  // Show the mouse cursor.
  ShowCursor(true);

  // Fix the display settings if leaving full screen mode.
  if (CurrentConfig._fullscreen)
  {
    ChangeDisplaySettings(NULL, 0);
  }

  // Remove the window.
  DestroyWindow(_windowHandle);
  _windowHandle = NULL;

  // Remove the application instance.
  UnregisterClass(PRTREE_PROGRAM_NAME, _instance);
  _instance = NULL;
}

Graphics* WindowsSystem::CreateGraphics()
{
  return new WindowsGraphics(this);
}

Input* WindowsSystem::CreateInput()
{
  return new WindowsInput(this);
}

FileSystem* WindowsSystem::CreateFileSystem()
{
  return new WindowsFileSystem;
}

PreciseTimer* WindowsSystem::CreatePreciseTimer()
{
  return new WindowsPreciseTimer;
}

WindowsGraphics& WindowsSystem::Graphics()
{
  return *static_cast<WindowsGraphics*>(_graphics);
}

WindowsInput& WindowsSystem::Input()
{
  return *static_cast<WindowsInput*>(_input);
}

WindowsFileSystem& WindowsSystem::FileSystem()
{
  return *static_cast<WindowsFileSystem*>(_fileSystem);
}

void WindowsSystem::Run()
{
  MSG msg;
  bool done;


  // Initialize the message structure.
  ZeroMemory(&msg, sizeof(MSG));

  // Loop until there is a quit message from the window or the user.
  done = false;
  while (!done)
  {
    // Handle the windows messages.
    if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
    {
      TranslateMessage(&msg);
      DispatchMessage(&msg);
    }

    // If windows signals to end the application then exit out.
    if (msg.message == WM_QUIT)
    {
      done = true;
    }
    else
    {
      done = !Frame();
    }
  }

  return;
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT umessage, WPARAM wparam, LPARAM lparam)
{
  switch (umessage)
  {
    // Check if the window is being destroyed.
    case WM_DESTROY:
    {
      PostQuitMessage(0);
      return 0;
    }

    // Check if the window is being closed.
    case WM_CLOSE:
    {
      PostQuitMessage(0);
      return 0;
    }

    // All other messages pass to the message handler in the system class.
    default:
    {
      return GWindowsSystem->MessageHandler(hwnd, umessage, wparam, lparam);
    }
  }
}

void WindowsSystem::SetupWindows()
{
  WNDCLASSEX wc;
  DEVMODE dmScreenSettings;
  int posX, posY;

  // Get the instance of this application.
  _instance = GetModuleHandle(NULL);

  // Setup the windows class with default settings.
  wc.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
  wc.lpfnWndProc = WndProc;
  wc.cbClsExtra = 0;
  wc.cbWndExtra = 0;
  wc.hInstance = _instance;
  wc.hIcon = LoadIcon(NULL, IDI_WINLOGO);
  wc.hIconSm = wc.hIcon;
  wc.hCursor = LoadCursor(NULL, IDC_ARROW);
  wc.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
  wc.lpszMenuName = NULL;
  wc.lpszClassName = PRTREE_PROGRAM_NAME;
  wc.cbSize = sizeof(WNDCLASSEX);

  // Register the window class.
  RegisterClassEx(&wc);

  // Determine the resolution of the clients desktop screen.
  int screenWidth = GetSystemMetrics(SM_CXSCREEN) < CurrentConfig._width ? GetSystemMetrics(SM_CXSCREEN) : CurrentConfig._width;
  int screenHeight = GetSystemMetrics(SM_CYSCREEN) < CurrentConfig._height ? GetSystemMetrics(SM_CYSCREEN) : CurrentConfig._height;

  // Setup the screen settings depending on whether it is running in full screen or in windowed mode.
  if (CurrentConfig._fullscreen)
  {
    // If full screen set the screen to maximum size of the users desktop and 32bit.
    memset(&dmScreenSettings, 0, sizeof(dmScreenSettings));
    dmScreenSettings.dmSize = sizeof(dmScreenSettings);
    dmScreenSettings.dmPelsWidth = (unsigned long)screenWidth;
    dmScreenSettings.dmPelsHeight = (unsigned long)screenHeight;
    dmScreenSettings.dmBitsPerPel = 32;
    dmScreenSettings.dmFields = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT;

    // Change the display settings to full screen.
    ChangeDisplaySettings(&dmScreenSettings, CDS_FULLSCREEN);

    // Set the position of the window to the top left corner.
    posX = posY = 0;
  }
  else
  {
    // Place the window in the higher top left of the screen.
    posX = (GetSystemMetrics(SM_CXSCREEN) - screenWidth) / 4;
    posY = (GetSystemMetrics(SM_CYSCREEN) - screenHeight) / 4;
  }

  // Create the window with the screen settings and get the handle to it.
  //_windowHandle = CreateWindowEx(WS_EX_APPWINDOW, PRTREE_PROGRAM_NAME, PRTREE_PROGRAM_NAME, WS_CLIPSIBLINGS | WS_CLIPCHILDREN | WS_POPUP,

  _windowHandle = CreateWindowEx(WS_EX_APPWINDOW, PRTREE_PROGRAM_NAME, PRTREE_PROGRAM_NAME, WS_CLIPSIBLINGS | WS_CLIPCHILDREN | WS_POPUP | WS_VISIBLE | WS_SYSMENU | WS_CAPTION | WS_THICKFRAME,
    posX, posY, screenWidth, screenHeight, NULL, NULL, _instance, NULL);

  // Bring the window up on the screen and set it as main focus.
  ShowWindow(_windowHandle, SW_SHOW);
  SetForegroundWindow(_windowHandle);
  SetFocus(_windowHandle);

  // Show/Hide the mouse cursor.
  ShowCursor(true);
}

void WindowsSystem::SetLowPriority()
{
  DWORD dwError, dwPriClass;
  if (!SetPriorityClass(GetCurrentProcess(), PROCESS_MODE_BACKGROUND_BEGIN))
  {
    dwError = GetLastError();
    if (ERROR_PROCESS_MODE_ALREADY_BACKGROUND == dwError)
      Error << "Already in background mode\n";
    else Error << "Failed to enter background mode (%d)\n";
  }

  // Display priority class

  dwPriClass = GetPriorityClass(GetCurrentProcess());

  Info << "Current priority class is " << (unsigned int) dwPriClass << "\n";


  if (!SetPriorityClass(GetCurrentProcess(), PROCESS_MODE_BACKGROUND_END))
  {
    Error << "Failed to end background mode (" << (unsigned int) GetLastError() << ")\n";
  }
}

LRESULT CALLBACK WindowsSystem::MessageHandler(HWND hwnd, UINT umsg, WPARAM wparam, LPARAM lparam)
{
  switch (umsg)
  {
    // Check if a key has been pressed on the keyboard.
    case WM_KEYDOWN:
    {
      // If a key is pressed send it to the input object so it can record that state.
      Input().OnWinKeyDown(static_cast<unsigned int>(wparam));
      return 0;
    }

    // Check if a key has been released on the keyboard.
    case WM_KEYUP:
    {
      // If a key is released then send it to the input object so it can unset the state for that key.
      Input().OnWinKeyUp(static_cast<unsigned int>(wparam));
      return 0;
    }

    // Any other messages send to the default message handler as our application won't make use of them.
    default:
    {
      return DefWindowProc(hwnd, umsg, wparam, lparam);
    }
  }
}

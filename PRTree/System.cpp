 #include "System.h"
#include "Log.h"

System::System()
  : _input(nullptr), _graphics(nullptr), _fileSystem(nullptr), _frameTimer(nullptr), _initialized(false), _frame(0), _lastFrameTime(1), _noGPU(false)
{

}

System::~System()
{
  delete _input;
  delete _graphics;
  delete _fileSystem;
  delete _frameTimer;
}

void System::Initialize(bool noGPU)
{
  OnInit();

  _noGPU = noGPU;

  _fileSystem = CreateFileSystem();
  _graphics = CreateGraphics();
  _input = CreateInput();
  _frameTimer = CreatePreciseTimer();

  _initialized = _graphics && _input;
}

void System::Shutdown()
{
  if (!Initialized())
    return;

  _initialized = false;

  OnShutdown();
}

float System::GetFrameTime()
{
  return _lastFrameTime;
}

bool System::Frame()
{
  if (Initialized())
  {
    _frameTimer->Reset();
    _input->Update();
    _graphics->Render();
    _lastFrameTime = _frameTimer->MeasureNow();
    return true;
  }
    
  return false;
}

#include <random>
#include "Lang.h"
#include "System.h"

static std::random_device rd;
static std::mt19937 e2(rd());
static std::uniform_real_distribution<> dist(0, 1);

void AssertPars(bool val, int lineNum, const char* filename, const char* expr)
{
  if (!val)
  {
    ExitWith("ASSERTION FAILED: " << filename << ":" << lineNum << " '" << expr << "'");
  }
}

void InitRandom()
{
  srand(time(nullptr));
}

float RandomFloatUnit()
{
  return static_cast<float>(dist(e2));
}

int RandomInt()
{
  return rand() * rand();
}


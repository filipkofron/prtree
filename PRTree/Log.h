#pragma once

#include <string>
#include <mutex>
#include <vector>
#include <sstream>

#ifdef ERROR
#undef ERROR
#endif // ERROR

#define LOG_TYPES(DECL) \
  DECL(LOG_ERROR) \
  DECL(LOG_WARN) \
  DECL(LOG_INFO) \
  DECL(LOG_DEBUG)

#define LOG_TYPE(type) type,

//! Log levels
enum class LogType : int
{
  LOG_TYPES(LOG_TYPE)
};

std::string LogTypeToString(const LogType& logType);

//! Interface for receiving a log
class LogReceiver
{
public:
  virtual void OnMessage(const std::string& msg, const LogType& type) = 0;
};

/**
 * Used to expand log types.
 * - 'XXss': converted by stringstream operator <<
 */
#define DEFINE_LOG_TYPE(XXss) \
  XXss(const char*) \
  XXss(const std::string&) \
  XXss(float) \
  XXss(double) \
  XXss(long double) \
  XXss(uint8_t) \
  XXss(uint16_t) \
  XXss(uint32_t) \
  XXss(uint64_t) \
  XXss(int8_t) \
  XXss(int16_t) \
  XXss(int32_t) \
  XXss(int64_t) \

//! Declares class friends for logger operator <<
#define LOGGER_OP_OUTPUT_FRIEND(type) \
  friend Logger& operator << (Logger& logger, type typeVal);

class Logger
{
  DEFINE_LOG_TYPE(LOGGER_OP_OUTPUT_FRIEND)

public:
  Logger(LogType logType);
  virtual ~Logger();

  static void AddLogReceiver(const std::shared_ptr<LogReceiver>& logReceiver);
  static void RemoveLogReceiver(const std::shared_ptr<LogReceiver>& logReceiver);
private:
  void Print(const std::string& str);
  LogType _logType;
  std::stringstream _ss;
  static std::mutex _logLock;
  static std::vector<std::shared_ptr<LogReceiver> > _logReceivers;
};

//! Declares operator << forward declaration
#define LOGGER_OP_OUTPUT_FORWARD(type) \
  Logger& operator << (Logger& logger, type typeVal);

DEFINE_LOG_TYPE(LOGGER_OP_OUTPUT_FORWARD)

extern Logger Error;
extern Logger Warn;
extern Logger Info;
extern Logger Debug;

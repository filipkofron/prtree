#include "TextureD3D11.h"
#include "System.h"
#include "StringUtils.h"

TextureD3D11::TextureD3D11(D3D11Dev& dev, const std::string& filename)
  : _source(filename)
{
  std::string checkedFilename = filename;
  if (checkedFilename.empty() || checkedFilename[checkedFilename.size() - 1] == '\\' || checkedFilename[checkedFilename.size() - 1] == '/')
    checkedFilename = "none.png";

  std::wstring wFilename = StringUtils::MultibyteStringToWide(checkedFilename);

  if (FAILED(DirectX::CreateWICTextureFromFile(&dev.GetDevice(), &dev.GetDeviceContext(),
    wFilename.c_str(), &_texture, &_textureResourceView)))
    ExitWith("Failed to load texture: " << checkedFilename);
}

TextureD3D11::~TextureD3D11()
{
}

void TextureD3D11::Apply(ID3D11DeviceContext& devContext)
{
  devContext.PSSetShaderResources(0, 1, _textureResourceView.GetAddressOf());
}

#pragma once

class WindowsSystem;

#include "WindowsCommon.h"
#include "System.h"
#include "WindowsGraphics.h"
#include "WindowsInput.h"
#include "WindowsFileSystem.h"
#include <string>
#include <chrono>

class WindowsSystem : public System
{
public:
  WindowsSystem();
  virtual ~WindowsSystem();
  virtual void debug_Sleep(uint64_t millis);
  virtual std::shared_ptr<LogReceiver> CreateDefaultLogReceiver();
  LRESULT CALLBACK MessageHandler(HWND hwnd, UINT umsg, WPARAM wparam, LPARAM lparam);
  HWND GetWindowHandle() const { return _windowHandle; }
  virtual void ExitWithError(std::function<void(std::ostream&)> msgFun);
  virtual float GetTimeFloat() override;

protected:
  virtual void OnInit() override;
  virtual void OnShutdown() override;
  virtual Graphics* CreateGraphics() override;
  virtual Input* CreateInput() override;
  virtual FileSystem* CreateFileSystem() override;
  virtual PreciseTimer* CreatePreciseTimer() override;
  WindowsGraphics& Graphics();
  WindowsInput& Input();
  WindowsFileSystem& FileSystem();
  virtual void Run();
private:
  HWND _windowHandle;
  HINSTANCE _instance;
  std::chrono::time_point<std::chrono::steady_clock> _startTime;

  void SetupWindows();
  void SetLowPriority();
};

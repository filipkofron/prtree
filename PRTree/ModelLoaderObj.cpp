#include "ModelLoaderObj.h"
#include "System.h"

ModelLoaderObj::ModelLoaderObj(const std::shared_ptr<ModelNativator>& modelNativator)
  : _modelNativator(modelNativator)
{

}

std::shared_ptr<ModelData> ModelLoaderObj::LoadModel(const std::string& source)
{
  auto& cached = _cachedModels.find(source);
  if (cached != _cachedModels.end())
    return cached->second;

  auto model = std::make_shared<ModelData>(source);
  if (!_modelNativator->DeserializeNative(*model))
  {
    std::string err;
    std::string matDir = GSystem->GetFileSystem()->GetDirectoryPath(source);
    if (!tinyobj::LoadObj(model->GetRawShapes(), model->GetRawMaterials(), err, source.c_str(), matDir.c_str()))
    {
      Debug << err << "\n";
    }

    Debug << "# of shapes    : " << model->GetRawShapes().size() << "\n";
    Debug << "# of materials : " << model->GetRawMaterials().size() << "\n";

    int i = 0;
    for (auto& shape : model->GetRawShapes())
    {
      Debug << "shape: " << i << "\n";
      Debug << "# indices: " << shape.mesh.indices.size() << "\n";
      Debug << "# positions: " << shape.mesh.positions.size() << "\n";
      Debug << "# normals: " << shape.mesh.normals.size() << "\n";
      Debug << "\n";
      i++;
    }

    /*
      std::string name;

  float ambient[3];
  float diffuse[3];
  float specular[3];
  float transmittance[3];
  float emission[3];
  float shininess;
  float ior;      // index of refraction
  float dissolve; // 1 == opaque; 0 == fully transparent
  // illumination model (see http://www.fileformat.info/format/material/)
  int illum;

  int dummy; // Suppress padding warning.

  std::string ambient_texname;            // map_Ka
  std::string diffuse_texname;            // map_Kd
  std::string specular_texname;           // map_Ks
  std::string specular_highlight_texname; // map_Ns
  std::string bump_texname;               // map_bump, bump
  std::string displacement_texname;       // disp
  std::string alpha_texname;              // map_d
  std::map<std::string, std::string> unknown_parameter;*/
    for (auto& mat : model->GetRawMaterials())
    {
      Debug << "mat: " << i << "\n";
      Debug << "# ambient_texname: " << mat.ambient_texname << "\n";
      Debug << "# diffuse_texname: " << mat.diffuse_texname << "\n";
      Debug << "# specular_texname: " << mat.specular_texname << "\n";
      Debug << "# specular_highlight_texname: " << mat.specular_highlight_texname << "\n";
      Debug << "# bump_texname: " << mat.bump_texname << "\n";
      Debug << "# displacement_texname: " << mat.displacement_texname << "\n";
      Debug << "# alpha_texname: " << mat.alpha_texname << "\n";
      for (const auto& unknown : mat.unknown_parameter)
      {
        Debug << "# unknown: " << unknown.first << ":" << unknown.second << "\n";
      }
      Debug << "\n";
      i++;
    }

    _modelNativator->ConvertToNative(*model);
    _modelNativator->SerializeNative(*model);
  }


  _cachedModels[source] = model;
  return model;
}

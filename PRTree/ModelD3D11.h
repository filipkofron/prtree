#pragma once

#include "Model.h"
#include "D3D11Dev.h"
#include "ShapeD3D11.h"

class ModelD3D11 : public Model
{
public:
  ModelD3D11(const std::shared_ptr<ModelLoader>& loader, const std::string& source, ID3D11Device& dev);
  virtual ~ModelD3D11();

  void Render(ID3D11DeviceContext& dev);
};

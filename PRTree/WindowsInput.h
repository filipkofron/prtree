#pragma once

class WindowsInput;

#include "WindowsCommon.h"
#include "Input.h"
#include "WindowsSystem.h"

#include <wrl.h>
using namespace Microsoft::WRL;

#define DIRECTINPUT_VERSION 0x0800
#include <dinput.h>

class WindowsInput : public Input
{
public:
  WindowsInput(WindowsSystem* system);

  void OnWinKeyDown(unsigned int key);
  void OnWinKeyUp(unsigned int key);
  void OnKeyPressed(unsigned int key);
  void OnKeyPressDown(unsigned int key);
  void HandleKey(unsigned int key);

  virtual void Update() override;
  bool ReadMouse();

private:
  void CheckKey(unsigned int key);
  static const int KEYS_MAX = 256;
  bool _keyPressed[KEYS_MAX];
  WindowsSystem* _system;

  ComPtr<IDirectInput8> _directInput;
  ComPtr<IDirectInputDevice8> _keyboard;
  ComPtr<IDirectInputDevice8> _mouse;

  unsigned char _keyboardState[256];
  DIMOUSESTATE _mouseState;
  int _mouseX, _mouseY;
};

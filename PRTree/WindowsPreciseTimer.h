#pragma once

#include "PreciseTimer.h"

#include "WindowsSystem.h"

class WindowsPreciseTimer : public PreciseTimer
{
private:
  double _pCFreq;
  __int64 _counterStart;
public:
  void Reset() override
  {
    LARGE_INTEGER li;
    QueryPerformanceFrequency(&li);
    _pCFreq = double(li.QuadPart) / 1000.0;
    QueryPerformanceCounter(&li);
    _counterStart = li.QuadPart;
  }

  double MeasureNow() const override
  {
    LARGE_INTEGER li;
    QueryPerformanceCounter(&li);
    return double(li.QuadPart - _counterStart) / _pCFreq;
  }
};

#include "SHLightingShaderD3D11.h"
#include "System.h"

SHLightingShaderD3D11::SHLightingShaderD3D11(ID3D11Device& dev, const std::string& name)
  : ShaderD3D11(dev, name.c_str())
{
  D3D11_SAMPLER_DESC samplerDesc;

  // Create a texture sampler state description.
  samplerDesc.Filter = D3D11_FILTER_MIN_MAG_MIP_LINEAR;
  samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
  samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
  samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
  samplerDesc.MipLODBias = 0.0f;
  samplerDesc.MaxAnisotropy = 1;
  samplerDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
  samplerDesc.BorderColor[0] = 0;
  samplerDesc.BorderColor[1] = 0;
  samplerDesc.BorderColor[2] = 0;
  samplerDesc.BorderColor[3] = 0;
  samplerDesc.MinLOD = 0;
  samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;

  // Create the texture sampler state.
  if (FAILED(dev.CreateSamplerState(&samplerDesc, &_samplerState)))
    ExitWith("Could not create sampler state!");

  D3D11_BLEND_DESC omDesc;
  ZeroMemory(&omDesc, sizeof(D3D11_BLEND_DESC));
  omDesc.RenderTarget[0].BlendEnable = true;
  omDesc.RenderTarget[0].SrcBlend = D3D11_BLEND_ONE;
  omDesc.RenderTarget[0].DestBlend = D3D11_BLEND_ZERO;
  omDesc.RenderTarget[0].BlendOp = D3D11_BLEND_OP_ADD;
  omDesc.RenderTarget[0].SrcBlendAlpha = D3D11_BLEND_ONE;
  omDesc.RenderTarget[0].DestBlendAlpha = D3D11_BLEND_ZERO;
  omDesc.RenderTarget[0].BlendOpAlpha = D3D11_BLEND_OP_ADD;
  omDesc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;
  omDesc.AlphaToCoverageEnable = true;

  if (FAILED(dev.CreateBlendState(&omDesc, &_blendState)))
    ExitWith("Cannot create blendstate!");
}

SHLightingShaderD3D11::~SHLightingShaderD3D11()
{

}

void SHLightingShaderD3D11::SetupLayout(ID3D11Device& dev)
{
  unsigned int numElements;

  // Now setup the layout of the data that goes into the shader.
  // This setup needs to match the VertexType structure in the ModelD3D11 and in the shader.

  D3D11_INPUT_ELEMENT_DESC polygonLayout[8] = 
  {
    { "POSITION",  0, DXGI_FORMAT_R32G32B32_FLOAT,    0,   0,                              D3D11_INPUT_PER_VERTEX_DATA,   0 },
    { "NORMAL",    0, DXGI_FORMAT_R32G32B32_FLOAT,    0,   D3D11_APPEND_ALIGNED_ELEMENT,   D3D11_INPUT_PER_VERTEX_DATA,   0 },
    { "TEXCOORD",  0, DXGI_FORMAT_R32G32_FLOAT,       0,   D3D11_APPEND_ALIGNED_ELEMENT,   D3D11_INPUT_PER_VERTEX_DATA,   0 },
    { "TEXCOORD",  1, DXGI_FORMAT_R32_UINT,           0,   D3D11_APPEND_ALIGNED_ELEMENT,   D3D11_INPUT_PER_VERTEX_DATA,   0 },
    { "COLOR",     0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0,   D3D11_APPEND_ALIGNED_ELEMENT,   D3D11_INPUT_PER_VERTEX_DATA,   0 },
    { "COLOR",     1, DXGI_FORMAT_R32G32B32A32_FLOAT, 0,   D3D11_APPEND_ALIGNED_ELEMENT,   D3D11_INPUT_PER_VERTEX_DATA,   0 },
    { "COLOR",     2, DXGI_FORMAT_R32G32B32A32_FLOAT, 0,   D3D11_APPEND_ALIGNED_ELEMENT,   D3D11_INPUT_PER_VERTEX_DATA,   0 },
    { "COLOR",     3, DXGI_FORMAT_R32G32B32A32_FLOAT, 0,   D3D11_APPEND_ALIGNED_ELEMENT,   D3D11_INPUT_PER_VERTEX_DATA,   0 },
  };

  // Get a count of the elements in the layout.
  numElements = sizeof(polygonLayout) / sizeof(polygonLayout[0]);

  // Create the vertex input layout.
  if (FAILED(dev.CreateInputLayout(polygonLayout, numElements, _vertexShaderBuffer->GetBufferPointer(),
    _vertexShaderBuffer->GetBufferSize(), &_layout)))
    ExitWith("Cannot create input layout");
}

void SHLightingShaderD3D11::OnApply(D3D11Dev& dev)
{
  dev.GetDeviceContext().OMSetBlendState(_blendState.Get(), 0, 0xffffffff);
}

cbuffer MatrixBuffer : register(b0)
{
  matrix worldMatrix;
  matrix viewMatrix;
  matrix projectionMatrix;
};

struct VertexInputType
{
  float4 position : POSITION;
  float4 normal : NORMAL0;
  float2 tex : TEXCOORD0;
  //float2 shapeUv : TEXCOORD1;
  uint material : TEXCOORD1;
  float4 shLight0 : COLOR0;
  float4 shLight1 : COLOR1;
  float4 shLight2 : COLOR2;
  float4 shLight3 : COLOR3;
};

struct PixelInputType
{
  float4 position : SV_POSITION;
  float4 normal : NORMAL0;
  float4 realPos : COORD0;
  float4 origPos : COORD1;
  float2 tex : TEXCOORD0;
  float material : TEXCOORD1;
  float3 shLight : COLOR0;
};

float Depth : SV_Depth;

struct PixelOutputType
{
  float4 outputColor : SV_TARGET;
  float depth : SV_Depth;
};
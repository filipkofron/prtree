#pragma once

/************************************************************************/
/* PRTree lighting prototype - Filip Kofron                             */
/************************************************************************/

#define PRTREE_PROGRAM_NAME   "PRTree"
#define PRTREE_VERSION_STRING "0.99"
#define PRTREE_AUTHOR         "Filip Kofron"


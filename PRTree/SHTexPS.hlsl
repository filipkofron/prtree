#include "SHTex.hlsli"

#define MATERIALS_MAX 32

Texture2DArray _ambientTextures : register(t0);
Texture2DArray _diffuseTextures : register(t1);
Texture2DArray _specularTextures : register(t2);
Texture2DArray _specularHighlightsTextures : register(t3);
Texture2DArray _bumpTextures : register(t6);
Texture2DArray _displacementTextures : register(t5);
Texture2DArray _alphaTextures : register(t6);
SamplerState linearSampler;
/*
struct MaterialType
{
  float3 ambient;
  float3 diffuse;
  float3 specular;
  float3 transmittance;
  float3 emission;
  float shininess;
  float dissolve;
  int ambientTextureIdx;
  int diffuseTextureIdx;
  int specularTextureIdx;
  int specularHighlightTextureIdx;
  int bumpTextureIdx;
  int displacementTextureIdx;
};

cbuffer MaterialLibrary : register(b1)
{
  MaterialType materials[16];
}*/

cbuffer MaterialLibrary : register(b1)
{
  float4 ambient[MATERIALS_MAX];
  float4 diffuse[MATERIALS_MAX];
  float4 specular[MATERIALS_MAX];
  float4 transmittance[MATERIALS_MAX];
  float4 emission[MATERIALS_MAX];
  float4 sdad[MATERIALS_MAX]; // [shininess coef, dissolve coef, ambient tex id, diffuse tex id]
  float4 ssbd[MATERIALS_MAX]; // [specular tex id, specural high tex id, bump tex id, displacement tex id]
  float4 flags[MATERIALS_MAX]; // [is leaf, none, none, none]
}

PixelOutputType main(PixelInputType input)
{
  int matIdx = int(input.material + 0.1f);
  //float3 final = diffuse[matIdx].xyz;
  float3 coords = float3(input.tex, input.material);

  float4 ambientTexture = _ambientTextures.Sample(linearSampler, coords);
  float4 diffuseTexture = _diffuseTextures.Sample(linearSampler, coords);
  float4 alphaTexture = _alphaTextures.Sample(linearSampler, coords);
  
  /*if (alphaTexture.r > 1e-5)
    discard;*/
//    result.depth = 1/0.0;

  float4 sunPos = float4(-10000.0, 10000.0, 2000.0, 1.0);
  sunPos = mul(sunPos, worldMatrix);
  float4 worldPos = mul(input.origPos, worldMatrix);
  float4 sunDir = normalize(sunPos - worldPos);
  float sunDiffuseCoef = dot(normalize(input.normal.xyz), sunDir.xyz);


  sunDiffuseCoef = clamp(sunDiffuseCoef, 0.0, 1.0);
  float transmitCoef = clamp(-sunDiffuseCoef, 0.0, 1.0);

  /*matrix worldView = viewMatrix * worldMatrix;
  float3x3 rotMat = 
  {
    worldView._m00, worldView._m01, worldView._m02,
    worldView._m10, worldView._m11, worldView._m12,
    worldView._m20, worldView._m21, worldView._m22,
  };

  float3 d = float3(worldView._m03, worldView._m13, worldView._m23);


  float3 eyePos = mul(rotMat, -d);

  float3 v = normalize(eyePos - worldPos);
  float3 r = reflect(normalize(sunDir.xyz), normalize(input.normal.xyz));
  float sunSpecCoef = pow(saturate(dot(r, v)), 2.0);*/

  //float4 result = textureColor * (sunDiffuseCoef + sunSpecCoef);
  PixelOutputType result;
  //result.outputColor.a = diffuseTexture.a;
  //result.outputColor.rgb = diffuseTexture.rgb;
   //result.outputColor.a = ambientTexture.a;
  //result.outputColor.a = 1.0 - alphaTexture.r;
  //result.outputColor.a = alphaTexture.r;
  
   //result.outputColor.rgb = ambientTexture;
   //result.outputColor.rgb = ambientTexture.rgb;

  // result.outputColor.rgb += diffuseTexture;
   result.outputColor.rgba = diffuseTexture.rgba;
   // result.outputColor.rgb = ambient[matIdx];
  // result.outputColor.rgb = float3(1.0, 1.0, 1.0) * input.material * 0.2;
  // result.outputColor.rgb = input.normal;
  // result.outputColor.xyz = diffuse[matIdx].xyz * sunDiffuseCoef;


  
  //result.outputColor.xyz = ambientTexture * ambient[matIdx].xyz * sunDiffuseCoef;
   //result.outputColor.xyz = ambientTexture * sunDiffuseCoef;
  //result.outputColor.xyz += diffuseTexture * diffuse[matIdx].xyz * sunDiffuseCoef;
  /*  result.outputColor.a = 1.0;
  */

   if ((result.outputColor.r + result.outputColor.g + result.outputColor.b) < 0.000001)
     result.outputColor.rgb = 0.1f;

   float3 dif = diffuse[matIdx];
   if (dif.r < 0.0001)
     dif = 1.0;

  float3 outCoef = ambient[matIdx] + input.shLight * dif;

  //result.outputColor.xyz *= clamp(outCoef, 0.0, 1.0);
  result.outputColor.xyz *= outCoef;


  // result.outputColor.xyz *= sunDiffuseCoef;
 //result.outputColor.xyz = diffuse[matIdx].xyz * sunDiffuseCoef;

  // result.outputColor.rgb = alphaTexture.rgb;
  
  
  /*result.outputColor.xyz = textureColor * diffuse[matIdx].xyz * sunDiffuseCoef;
  result.outputColor.xyz += transmitCoef * float3(1.0, 1.0, 1.0);// *transmittance[matIdx].xyz;
  result.outputColor.xyz += textureColor * (ambient[matIdx].xyz + 0.3);
  */
  // result = _diffuseTextures.Sample(linearSampler, float3(input.tex, input.material + 1));

   //result.outputColor = float4(normalize(input.normal.xyz), 1.0);
  result.depth = input.realPos.z / input.realPos.w;
  result.outputColor.rgb *= 2;
  return result;
}

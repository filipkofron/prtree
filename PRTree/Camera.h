#pragma once

class Camera
{
public:
  virtual void Translate(float x, float y, float z) = 0;
  virtual void Rotate(float x, float y, float z) = 0;
  virtual void TranslateSmooth(float x, float y, float z, float time) = 0;
  virtual void RotateSmooth(float x, float y, float z, float time) = 0;
};

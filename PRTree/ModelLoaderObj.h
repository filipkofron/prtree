#pragma once

class ModelLoaderObj;

#include "ModelLoader.h"
#include "ModelNativator.h"
#include "tiny_obj_loader.h"

class ModelLoaderObj : public ModelLoader
{
public:
  ModelLoaderObj(const std::shared_ptr<ModelNativator>& modelNativator);
  virtual std::shared_ptr<ModelData> LoadModel(const std::string& source) override;
protected:
  std::shared_ptr<ModelNativator> _modelNativator;
};

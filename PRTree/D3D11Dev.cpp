#include "D3D11Dev.h"
#include "WindowsCommon.h"
#include "Log.h"
#include "Config.h"
#include "System.h"
#include "StringUtils.h"
#include <cstring>
#include <stdlib.h>

D3D11Dev::D3D11Dev(HWND hwnd)
  : _hwnd(hwnd), _deviceDebug(nullptr), _textureManager(std::make_unique<TextureManagerD3D11>(*this))
{
  Debug << "D3D11 Init." << "\n";
  HRESULT result;
  IDXGIFactory* factory;
  IDXGIAdapter* adapter;
  IDXGIAdapter* primaryAdapter;
  IDXGIOutput* adapterOutput;
  unsigned int numModes, i, numerator = CurrentConfig._width, denominator = CurrentConfig._height;
  DXGI_MODE_DESC* displayModeList;
  DXGI_ADAPTER_DESC adapterDesc;
  DXGI_SWAP_CHAIN_DESC swapChainDesc;
  D3D_FEATURE_LEVEL featureLevel;
  ID3D11Texture2D* backBufferPtr;
  D3D11_TEXTURE2D_DESC depthBufferDesc;
  D3D11_DEPTH_STENCIL_DESC depthStencilDesc;
  D3D11_DEPTH_STENCIL_VIEW_DESC depthStencilViewDesc;
  D3D11_RASTERIZER_DESC rasterDesc;
  D3D11_VIEWPORT viewport;
  D3D11_BUFFER_DESC matrixBufferDesc;

  // Create a DirectX graphics interface factory.
  result = CreateDXGIFactory(__uuidof(IDXGIFactory), (void**)&factory);
  if (FAILED(result))
    ExitWith("Cannot create DirectX Interface!");

  // Use the factory to create an adapter for the primary graphics interface (video card).

  int adapterIdx = 0;
  int bestAdapterIdx = 0;
  size_t dedicatedMem = 0;
  result = factory->EnumAdapters(0, &primaryAdapter);
  if (FAILED(result))
  {
    Warn << "Cannot get primary adapter!";
    goto haxorSkip;
  }
  do 
  {
    result = factory->EnumAdapters(adapterIdx, &adapter);
    if (!FAILED(result))
    {
      result = adapter->GetDesc(&adapterDesc);
      if (!FAILED(result))
      {
        if (adapterDesc.DedicatedVideoMemory > dedicatedMem)
        {
          dedicatedMem = adapterDesc.DedicatedVideoMemory;
          bestAdapterIdx = adapterIdx;
        }
      }
      adapterIdx++;
    }
  } while (!FAILED(result));
  result = factory->EnumAdapters(bestAdapterIdx, &adapter);

  if (FAILED(result))
    ExitWith("Cannot enumerate adapters!");

  // Enumerate the primary adapter output (monitor).
  result = primaryAdapter->EnumOutputs(0, &adapterOutput);
  if (FAILED(result))
    ExitWith("Cannot get primary adapter output!");

  // Get the number of modes that fit the DXGI_FORMAT_R8G8B8A8_UNORM display format for the adapter output (monitor).
  result = adapterOutput->GetDisplayModeList(DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_ENUM_MODES_INTERLACED, &numModes, NULL);
  if (FAILED(result))
  {
    Warn << "Cannot get display mode list size!" << "\n";
    Warn << "Using preset config resolution of "
      << CurrentConfig._width << "x" << CurrentConfig._height  << "\n";
    numModes = 1;
    displayModeList = new DXGI_MODE_DESC[numModes];
    displayModeList[0].Width = CurrentConfig._width;
    displayModeList[0].Height = CurrentConfig._height;
  }
  else
  {
    // Create a list to hold all the possible display modes for this monitor/video card combination.
    displayModeList = new DXGI_MODE_DESC[numModes];

    // Now fill the display mode list structures.
    result = adapterOutput->GetDisplayModeList(DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_ENUM_MODES_INTERLACED, &numModes, displayModeList);
    if (FAILED(result))
      ExitWith("Cannot get display mode list!");
  }

  

  // Now go through all the display modes and find the one that matches the screen width and height.
  // When a match is found store the numerator and denominator of the refresh rate for that monitor.
  for (i = 0; i < numModes; i++)
  {
    if (displayModeList[i].Width == (unsigned int) CurrentConfig._width)
    {
      if (displayModeList[i].Height == (unsigned int) CurrentConfig._height)
      {
        numerator = displayModeList[i].RefreshRate.Numerator;
        denominator = displayModeList[i].RefreshRate.Denominator;
      }
    }
  }
  
  // Get the adapter (video card) description.
  result = adapter->GetDesc(&adapterDesc);
  if (FAILED(result))
    ExitWith("Cannot get video card description!");

  // Store the dedicated video card memory in megabytes.
  _videoCardMemory = (int)(adapterDesc.DedicatedVideoMemory / 1024 / 1024);

  // Convert the name of the video card to a character array and store it.
  _videoCardDescription = StringUtils::WideStringToMultibyte(adapterDesc.Description);

  // Release the display mode list.
  delete[] displayModeList;
  // Release the adapter output.
  adapterOutput->Release();
  // Release the adapter.
  adapter->Release();
  // Release the factory.
  factory->Release();
haxorSkip:
  // Initialize the swap chain description.
  ZeroMemory(&swapChainDesc, sizeof(swapChainDesc));

  // Set to a single back buffer.
  swapChainDesc.BufferCount = 1;

  // Set the width and height of the back buffer.
  swapChainDesc.BufferDesc.Width = CurrentConfig._width;
  swapChainDesc.BufferDesc.Height = CurrentConfig._height;

  // Set regular 32-bit surface for the back buffer.
  //swapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
  swapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;

  // Set the refresh rate of the back buffer.
  if (CurrentConfig._vsync)
  {
    swapChainDesc.BufferDesc.RefreshRate.Numerator = numerator;
    swapChainDesc.BufferDesc.RefreshRate.Denominator = denominator;
  }
  else
  {
    swapChainDesc.BufferDesc.RefreshRate.Numerator = 0;
    swapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
  }

  // Set the usage of the back buffer.
  swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;

  // Set the handle for the window to render to.
  swapChainDesc.OutputWindow = hwnd;

  // Turn multisampling off.
  swapChainDesc.SampleDesc.Count = 4;
  swapChainDesc.SampleDesc.Quality = 0;

  // Set to full screen or windowed mode.
  swapChainDesc.Windowed = !CurrentConfig._fullscreen;

  // Set the scan line ordering and scaling to unspecified.
  swapChainDesc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
  swapChainDesc.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;

  // Discard the back buffer contents after presenting.
  swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;

  // Don't set the advanced flags.
  swapChainDesc.Flags = 0;

  // Set the feature level to DirectX 11.
  featureLevel = D3D_FEATURE_LEVEL_11_0;

  int flags = 0;
#if _DEBUG
  flags |= D3D11_CREATE_DEVICE_DEBUG;
#endif
  //D3D_DRIVER_TYPE_WARP
  //D3D_DRIVER_TYPE_HARDWARE
  D3D_DRIVER_TYPE driverType = D3D_DRIVER_TYPE_HARDWARE;
  if (GSystem->IsNoGPU())
  {
    driverType = D3D_DRIVER_TYPE_WARP;
  }
  // Create the swap chain, Direct3D device, and Direct3D device context.
  result = D3D11CreateDeviceAndSwapChain(NULL, driverType, NULL, flags, &featureLevel, 1,
    D3D11_SDK_VERSION, &swapChainDesc, &_swapChain, &_device, NULL, &_deviceContext);
  if (FAILED(result))
    ExitWith("Failed to create device and swap chain!");

  _device->QueryInterface(IID_PPV_ARGS(&_deviceDebug));

  // dump output only if we actually grabbed a debug interface
  if (_deviceDebug != nullptr)
  {
    _deviceDebug->ReportLiveDeviceObjects(D3D11_RLDO_DETAIL);
  }

  // Get the pointer to the back buffer.
  result = _swapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&backBufferPtr);
  if (FAILED(result))
    ExitWith("Failed to retrieve the swap chain back buffer!");

  // Create the render target view with the back buffer pointer.
  result = _device->CreateRenderTargetView(backBufferPtr, NULL, &_renderTargetView);
  if (FAILED(result))
    ExitWith("Failed to create render target view!");

  // Release pointer to the back buffer as we no longer need it.
  backBufferPtr->Release();
  backBufferPtr = 0;

  // Initialize the description of the depth buffer.
  ZeroMemory(&depthBufferDesc, sizeof(depthBufferDesc));

  // Set up the description of the depth buffer.
  depthBufferDesc.Width = CurrentConfig._width;
  depthBufferDesc.Height = CurrentConfig._height;
  depthBufferDesc.MipLevels = 1;
  depthBufferDesc.ArraySize = 1;
  depthBufferDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
  depthBufferDesc.SampleDesc.Count = 4;
  depthBufferDesc.SampleDesc.Quality = 0;
  depthBufferDesc.Usage = D3D11_USAGE_DEFAULT;
  depthBufferDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
  depthBufferDesc.CPUAccessFlags = 0;
  depthBufferDesc.MiscFlags = 0;
  // Create the texture for the depth buffer using the filled out description.
  result = _device->CreateTexture2D(&depthBufferDesc, NULL, &_depthStencilBuffer);
  if (FAILED(result))
    ExitWith("Failed to create texture for depth buffer!");

  // Initialize the description of the stencil state.
  ZeroMemory(&depthStencilDesc, sizeof(depthStencilDesc));

  // Set up the description of the stencil state.
  depthStencilDesc.DepthEnable = true;
  depthStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
  depthStencilDesc.DepthFunc = D3D11_COMPARISON_LESS;

  depthStencilDesc.StencilEnable = true;
  depthStencilDesc.StencilReadMask = 0xFF;
  depthStencilDesc.StencilWriteMask = 0xFF;

  // Stencil operations if pixel is front-facing.
  depthStencilDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
  depthStencilDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_INCR;
  depthStencilDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
  depthStencilDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

  // Stencil operations if pixel is back-facing.
  depthStencilDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
  depthStencilDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_DECR;
  depthStencilDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
  depthStencilDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;

  // Create the depth stencil state.
  result = _device->CreateDepthStencilState(&depthStencilDesc, &_depthStencilState);
  if (FAILED(result))
    ExitWith("Failed to create depth stencil state!");

  // Set the depth stencil state.
  _deviceContext->OMSetDepthStencilState(_depthStencilState, 1);
  // Initailze the depth stencil view.
  ZeroMemory(&depthStencilViewDesc, sizeof(depthStencilViewDesc));

  // Set up the depth stencil view description.
  depthStencilViewDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
  depthStencilViewDesc.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2DMS;
  depthStencilViewDesc.Texture2D.MipSlice = 0;

  // Create the depth stencil view.
  result = _device->CreateDepthStencilView(_depthStencilBuffer, &depthStencilViewDesc, &_depthStencilView);
  if (FAILED(result))
    ExitWith("Failed to create depth stencil view!");
  // Bind the render target view and depth stencil buffer to the output render pipeline.
  _deviceContext->OMSetRenderTargets(1, &_renderTargetView, _depthStencilView);

  // Setup the raster description which will determine how and what polygons will be drawn.
  rasterDesc.AntialiasedLineEnable = false;
 // rasterDesc.CullMode = D3D11_CULL_BACK;
  rasterDesc.CullMode = D3D11_CULL_NONE;
  rasterDesc.DepthBias = 0;
  rasterDesc.DepthBiasClamp = 0.0f;
  rasterDesc.DepthClipEnable = true;
  rasterDesc.FillMode = D3D11_FILL_SOLID;
  rasterDesc.FrontCounterClockwise = false;
  rasterDesc.MultisampleEnable = true;
  rasterDesc.ScissorEnable = false;
  rasterDesc.SlopeScaledDepthBias = 0.0f;

  // Create the rasterizer state from the description we just filled out.
  result = _device->CreateRasterizerState(&rasterDesc, &_rasterState);
  if (FAILED(result))
    ExitWith("Failed to create rasterizer state!");

  // Now set the rasterizer state.
  _deviceContext->RSSetState(_rasterState);

  // Setup the viewport for rendering.
  viewport.Width = (float) CurrentConfig._width;
  viewport.Height = (float) CurrentConfig._height;
  viewport.MinDepth = 0.0f;
  viewport.MaxDepth = 1.0f;
  viewport.TopLeftX = 0.0f;
  viewport.TopLeftY = 0.0f;

  // Create the viewport.
  _deviceContext->RSSetViewports(1, &viewport);

  // Setup the description of the dynamic matrix constant buffer that is in the vertex shader.
  matrixBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
  matrixBufferDesc.ByteWidth = sizeof(MatrixBufferType);
  matrixBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
  matrixBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
  matrixBufferDesc.MiscFlags = 0;
  matrixBufferDesc.StructureByteStride = 0;

  // Create the constant buffer pointer so we can access the vertex shader constant buffer from within this class.
  result = _device->CreateBuffer(&matrixBufferDesc, NULL, &_matrixBuffer);
  if (FAILED(result))
    ExitWith("Could not allocate matrix buffer!");
}

void D3D11Dev::InitMatrices(float fov, float aspect, float zNear, float zFar)
{
  // Create the projection matrix for 3D rendering.
  _projectionMatrix = DirectX::XMMatrixPerspectiveFovLH(fov, aspect, zNear, zFar);

  // Initialize the world matrix to the identity matrix.
  _worldMatrix = DirectX::XMMatrixIdentity();

  // Create an orthographic projection matrix for 2D rendering.
  _orthoMatrix = DirectX::XMMatrixOrthographicLH((float) CurrentConfig._width, (float) CurrentConfig._height, zNear, zFar);
}

ID3D11Device& D3D11Dev::GetDevice()
{
  return *_device;
}

ID3D11DeviceContext& D3D11Dev::GetDeviceContext()
{
  return *_deviceContext;
}

void D3D11Dev::ApplyMatrices(CameraD3D11& camera)
{
  HRESULT result;
  D3D11_MAPPED_SUBRESOURCE mappedResource;
  MatrixBufferType* dataPtr;
  unsigned int bufferNumber;
  
  DirectX::XMMATRIX worldMatrix= DirectX::XMMatrixTranspose(_worldMatrix);
  DirectX::XMMATRIX viewMatrix = DirectX::XMMatrixTranspose(camera.GetViewMatrix());
  DirectX::XMMATRIX projectionMatrix = DirectX::XMMatrixTranspose(_projectionMatrix);
  
  // Lock the constant buffer so it can be written to.
  result = _deviceContext->Map(_matrixBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
  if (FAILED(result))
    ExitWith("Could not lock the matrix buffer");

  // Get a pointer to the data in the constant buffer.
  dataPtr = (MatrixBufferType*) mappedResource.pData;

  // Copy the matrices into the constant buffer.
  dataPtr->_world = worldMatrix;
  dataPtr->_view = viewMatrix;
  dataPtr->_projection = projectionMatrix;

  // Unlock the constant buffer.
  _deviceContext->Unmap(_matrixBuffer, 0);

  // Set the position of the constant buffer in the vertex shader.
  bufferNumber = 0;

  // Finally set the constant buffer in the vertex shader with the updated values.
  _deviceContext->VSSetConstantBuffers(bufferNumber, 1, &_matrixBuffer);
  _deviceContext->PSSetConstantBuffers(bufferNumber, 1, &_matrixBuffer);
}

void ReadBuffer(uint8_t* dst, ID3D11Buffer* src, const D3D11_BUFFER_DESC& desc, D3D11Dev& dev)
{
  D3D11_BUFFER_DESC bufferDescCPU;
  D3D11_BUFFER_DESC bufferDescGPU = desc;

  ComPtr<ID3D11Buffer> tempVertexBufferCPU = nullptr;

  memset(&bufferDescCPU, 0, sizeof(bufferDescCPU));
  bufferDescCPU = bufferDescGPU;
  bufferDescCPU.Usage = D3D11_USAGE_STAGING;
  bufferDescCPU.CPUAccessFlags = D3D11_CPU_ACCESS_READ | D3D11_CPU_ACCESS_WRITE;
  bufferDescCPU.BindFlags = 0;
  bufferDescCPU.StructureByteStride = bufferDescGPU.StructureByteStride;
  HRESULT res = dev.GetDevice().CreateBuffer(&bufferDescCPU, nullptr, tempVertexBufferCPU.GetAddressOf());
  if (FAILED(res))
    ExitWith("Could not allocate cpu buffer for shape data!");
  dev.GetDeviceContext().CopyResource(tempVertexBufferCPU.Get(), src);
  D3D11_MAPPED_SUBRESOURCE ms;
  dev.GetDeviceContext().Map(tempVertexBufferCPU.Get(), 0, D3D11_MAP_READ, 0, &ms);
  memcpy(dst, ms.pData, bufferDescCPU.ByteWidth);
  dev.GetDeviceContext().Unmap(tempVertexBufferCPU.Get(), 0);
}

void D3D11Dev::SerializeBuffer(std::ostream& output, ID3D11Buffer& buffer)
{
  D3D11_BUFFER_DESC bufferDescGPU;
  uint8_t* pureData;

  buffer.GetDesc(&bufferDescGPU);
  pureData = new uint8_t[bufferDescGPU.ByteWidth];
  ReadBuffer(pureData, &buffer, bufferDescGPU, *this);
  size_t size = static_cast<size_t>(bufferDescGPU.ByteWidth);
  output.write(reinterpret_cast<const char*>(&size), sizeof(size));
  output.write(reinterpret_cast<const char*>(pureData), bufferDescGPU.ByteWidth);

  delete[] pureData;
}

void D3D11Dev::ReadBufferToRaw(ID3D11Buffer& buffer, std::vector<float>& dest)
{
  D3D11_BUFFER_DESC bufferDescGPU;
  uint8_t* pureData;

  buffer.GetDesc(&bufferDescGPU);
  dest.resize(bufferDescGPU.ByteWidth);
  ReadBuffer(reinterpret_cast<uint8_t*>(dest.data()), &buffer, bufferDescGPU, *this);
}

void D3D11Dev::ReadBufferToRaw(ID3D11Buffer& buffer, std::vector<int>& dest)
{
  D3D11_BUFFER_DESC bufferDescGPU;
  uint8_t* pureData;

  buffer.GetDesc(&bufferDescGPU);
  dest.resize(bufferDescGPU.ByteWidth);
  ReadBuffer(reinterpret_cast<uint8_t*>(dest.data()), &buffer, bufferDescGPU, *this);
}

D3D11Dev::~D3D11Dev()
{
  Debug << "D3D11 Deinit." << '\n';

  // Release the matrix constant buffer.
  if (_matrixBuffer)
  {
    _matrixBuffer->Release();
    _matrixBuffer = 0;
  }

  // Before shutting down set to windowed mode or when you release the swap chain it will throw an exception.
  if (_swapChain)
    _swapChain->SetFullscreenState(false, NULL);

  if (_rasterState)
  {
    _rasterState->Release();
    _rasterState = 0;
  }

  if (_depthStencilView)
  {
    _depthStencilView->Release();
    _depthStencilView = 0;
  }

  if (_depthStencilState)
  {
    _depthStencilState->Release();
    _depthStencilState = 0;
  }

  if (_depthStencilBuffer)
  {
    _depthStencilBuffer->Release();
    _depthStencilBuffer = 0;
  }

  if (_renderTargetView)
  {
    _renderTargetView->Release();
    _renderTargetView = 0;
  }

  if (_deviceDebug)
  {
    _deviceDebug->Release();
    _deviceDebug = 0;
  }

  if (_deviceContext)
  {
    _deviceContext->Release();
    _deviceContext = 0;
  }

  if (_device)
  {
    _device->Release();
    _device = 0;
  }

  if (_swapChain)
  {
    _swapChain->Release();
    _swapChain = 0;
  }

}

void D3D11Dev::BeginScene(float red, float green, float blue, float alpha)
{
  float color[4];
  // Setup the color to clear the buffer to.
  color[0] = red;
  color[1] = green;
  color[2] = blue;
  color[3] = alpha;

  // Clear the back buffer.
  _deviceContext->ClearRenderTargetView(_renderTargetView, color);

  // Clear the depth buffer.
  _deviceContext->ClearDepthStencilView(_depthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0);
}

void D3D11Dev::EndScene()
{
  // Present the back buffer to the screen since rendering is complete.
  if (CurrentConfig._vsync)
  {
    // Lock to screen refresh rate.
    _swapChain->Present(1, 0);
  }
  else
  {
    // Present as fast as possible.
    _swapChain->Present(0, 0);
  }
}

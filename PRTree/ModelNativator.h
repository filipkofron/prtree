#pragma once

class ModelNativator;

#include "Shape.h"
#include "ModelData.h"

class ModelNativator
{
public:
  virtual void ConvertToNative(ModelData& modelData) = 0;
  virtual bool DeserializeNative(ModelData& modelData) = 0;
  virtual void SerializeNative(const ModelData& modelData) = 0;
};

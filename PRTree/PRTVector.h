#pragma once

#include <cmath>

struct PRTFloat4
{
  float _data[4];
};

struct PRTVector3
{
  float _x, _y, _z;

  PRTVector3 CrossProduct(const PRTVector3& prtVec) const
  {
    PRTVector3 res;
    res[0] = (*this)[1] * prtVec[2] - (*this)[2] * prtVec[1];
    res[1] = (*this)[2] * prtVec[0] - (*this)[0] * prtVec[2];
    res[2] = (*this)[0] * prtVec[1] - (*this)[1] * prtVec[0];
    return res;
  }

  PRTVector3 Add(const PRTVector3& prtVec) const
  {
    PRTVector3 res;
    res[0] = (*this)[0] + prtVec[0];
    res[1] = (*this)[1] + prtVec[1];
    res[2] = (*this)[2] + prtVec[2];
    return res;
  }

  PRTVector3 Sub(const PRTVector3& prtVec) const
  {
    PRTVector3 res;
    res[0] = (*this)[0] - prtVec[0];
    res[1] = (*this)[1] - prtVec[1];
    res[2] = (*this)[2] - prtVec[2];
    return res;
  }

  PRTVector3 Mul(const float& a) const
  {
    PRTVector3 res;
    res[0] = (*this)[0] - a;
    res[1] = (*this)[1] - a;
    res[2] = (*this)[2] - a;
    return res;
  }

  float DotProduct(const PRTVector3& prtVec) const
  {
    return (*this)[0] * prtVec[0]
         + (*this)[1] * prtVec[1]
         + (*this)[2] * prtVec[2];
  }

  float Min(float left, float right) const
  {
    return left <= right ? left : right;
  }

  float Max(float left, float right) const
  {
    return left >= right ? left : right;
  }

  PRTVector3 Min(const PRTVector3& right) const
  {
    PRTVector3 ret;
    ret._x = Min(_x, right._x);
    ret._y = Min(_y, right._y);
    ret._z = Min(_z, right._z);
    return ret;
  }

  PRTVector3 Max(const PRTVector3& right) const
  {
    PRTVector3 ret;
    ret._x = Max(_x, right._x);
    ret._y = Max(_y, right._y);
    ret._z = Max(_z, right._z);
    return ret;
  }
  
  PRTVector3 Avg(const PRTVector3& right) const
  {
    PRTVector3 ret;
    ret._x = (_x + right._x) * 0.5f;
    ret._y = (_y + right._y) * 0.5f;
    ret._z = (_z + right._z) * 0.5f;
    return ret;
  }

  float& operator[] (int i)
  {
    switch (i)
    {
    case 0:
      return _x;
    case 1:
      return _y;
    default:
      return _z;
    }
  }

  const float& operator[] (int i) const
  {
    switch (i)
    {
    case 0:
      return _x;
    case 1:
      return _y;
    default:
      return _z;
    }
  }

  bool Compare(const PRTVector3& vec, float eps) const
  {
    return abs(_x - vec._x) < eps
        && abs(_y - vec._y) < eps
        && abs(_z - vec._z) < eps;
  }
};

struct PRTVector3Int
{
  int _x, _y, _z;
};

struct PRTVector2
{
  float _x, _y;
};

#pragma once

class TextureManagerD3D11;
class TextureD3D11;

#include "D3D11Dev.h"
#include <map>
#include <memory>
#include <string>

class TextureManagerD3D11
{
public:
  TextureManagerD3D11(D3D11Dev& dev) : _d3d11Dev(dev) { }
  std::shared_ptr<TextureD3D11> GetTexture(const std::string& path);
private:
  std::map<std::string, std::shared_ptr<TextureD3D11> > _cachedTextures;
  D3D11Dev& _d3d11Dev;
};

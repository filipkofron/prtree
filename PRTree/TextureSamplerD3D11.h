#pragma once

class TextureSamplerD3D11;

#include "D3D11Dev.h"

class TextureSamplerD3D11
{
public:
  TextureSamplerD3D11(D3D11Dev& dev);
  void Apply(ID3D11DeviceContext& devContext);
  ID3D11SamplerState* GetSampler() const { return _sampler.Get(); }
protected:
  ComPtr<ID3D11SamplerState> _sampler;
};

#pragma once

#include "Lang.h"
#include <stdint.h>

#include <cmath>
#include <emmintrin.h>
#include <pmmintrin.h>

#define SHUFFLE3_M128(V, X,Y,Z) Float3(_mm_shuffle_ps((V)._m128, (V)._m128, _MM_SHUFFLE(Z,Z,Y,X)))

#define VCONST extern const __declspec(selectany)
struct vconstu
{
  union { uint32_t u[4]; __m128 v; };
  INLINE operator __m128() const { return v; }
};

VCONST vconstu vsignbits = { 0x80000000, 0x80000000, 0x80000000, 0x80000000 };

struct Float3
{
  __m128 _m128;

  INLINE explicit Float3(const float *p) { _m128 = _mm_set_ps(0, p[2], p[1], p[0]); }
  INLINE explicit Float3(float x, float y, float z) { _m128 = _mm_set_ps(0, z, y, x); }
  INLINE explicit Float3(__m128 v) { _m128 = v; }

  __m128& M128() { return _m128; }
  const __m128& M128() const { return _m128; }

  INLINE Float3 YZX() const { return SHUFFLE3_M128(*this, 1, 2, 0); }
  INLINE Float3 ZXY() const { return SHUFFLE3_M128(*this, 2, 0, 1); }

  INLINE float X() const { return _mm_cvtss_f32(_m128); }
  INLINE float Y() const { return _mm_cvtss_f32(_mm_shuffle_ps(_m128, _m128, _MM_SHUFFLE(1, 1, 1, 1))); }
  INLINE float Z() const { return _mm_cvtss_f32(_mm_shuffle_ps(_m128, _m128, _MM_SHUFFLE(2, 2, 2, 2))); }

  INLINE void Store(float *p) const { p[0] = X(); p[1] = Y(); p[2] = Z(); }

  INLINE void setX(float x)
  {
    _m128 = _mm_move_ss(_m128, _mm_set_ss(x));
  }
  INLINE void setY(float y)
  {
    __m128 t = _mm_move_ss(_m128, _mm_set_ss(y));
    t = _mm_shuffle_ps(t, t, _MM_SHUFFLE(3, 2, 0, 0));
    _m128 = _mm_move_ss(t, _m128);
  }
  INLINE void setZ(float z)
  {
    __m128 t = _mm_move_ss(_m128, _mm_set_ss(z));
    t = _mm_shuffle_ps(t, t, _MM_SHUFFLE(3, 0, 1, 0));
    _m128 = _mm_move_ss(t, _m128);
  }
};

INLINE Float3 operator+ (Float3 a, Float3 b) { a._m128 = _mm_add_ps(a._m128, b._m128); return a; }
INLINE Float3 operator- (Float3 a, Float3 b) { a._m128 = _mm_sub_ps(a._m128, b._m128); return a; }
INLINE Float3 operator* (Float3 a, Float3 b) { a._m128 = _mm_mul_ps(a._m128, b._m128); return a; }
INLINE Float3 operator/ (Float3 a, Float3 b) { a._m128 = _mm_div_ps(a._m128, b._m128); return a; }
INLINE Float3 operator* (Float3 a, float b) { a._m128 = _mm_mul_ps(a._m128, _mm_set1_ps(b)); return a; }
INLINE Float3 operator/ (Float3 a, float b) { a._m128 = _mm_div_ps(a._m128, _mm_set1_ps(b)); return a; }
INLINE Float3 operator* (float a, Float3 b) { b._m128 = _mm_mul_ps(_mm_set1_ps(a), b._m128); return b; }
INLINE Float3 operator/ (float a, Float3 b) { b._m128 = _mm_div_ps(_mm_set1_ps(a), b._m128); return b; }
INLINE Float3& operator+= (Float3 &a, Float3 b) { a = a + b; return a; }
INLINE Float3& operator-= (Float3 &a, Float3 b) { a = a - b; return a; }
INLINE Float3& operator*= (Float3 &a, Float3 b) { a = a * b; return a; }
INLINE Float3& operator/= (Float3 &a, Float3 b) { a = a / b; return a; }
INLINE Float3& operator*= (Float3 &a, float b) { a = a * b; return a; }
INLINE Float3& operator/= (Float3 &a, float b) { a = a / b; return a; }
INLINE Float3 operator==(Float3 a, Float3 b) { a._m128 = _mm_cmpeq_ps(a._m128, b._m128); return a; }
INLINE Float3 operator!=(Float3 a, Float3 b) { a._m128 = _mm_cmpneq_ps(a._m128, b._m128); return a; }
INLINE Float3 operator< (Float3 a, Float3 b) { a._m128 = _mm_cmplt_ps(a._m128, b._m128); return a; }
INLINE Float3 operator> (Float3 a, Float3 b) { a._m128 = _mm_cmpgt_ps(a._m128, b._m128); return a; }
INLINE Float3 operator<=(Float3 a, Float3 b) { a._m128 = _mm_cmple_ps(a._m128, b._m128); return a; }
INLINE Float3 operator>=(Float3 a, Float3 b) { a._m128 = _mm_cmpge_ps(a._m128, b._m128); return a; }
INLINE Float3 Min(Float3 a, Float3 b) { a._m128 = _mm_min_ps(a._m128, b._m128); return a; }
INLINE Float3 Max(Float3 a, Float3 b) { a._m128 = _mm_max_ps(a._m128, b._m128); return a; }

INLINE Float3 operator- (Float3 a) { return Float3(_mm_setzero_ps()) - a; }
INLINE Float3 Abs(Float3 v) { v._m128 = _mm_andnot_ps(vsignbits, v._m128); return v; }

INLINE float HMin(Float3 v)
{
  v = Min(v, SHUFFLE3_M128(v, 1, 0, 2));
  return Min(v, SHUFFLE3_M128(v, 2, 0, 1)).X();
}

INLINE float HMax(Float3 v)
{
  v = Max(v, SHUFFLE3_M128(v, 1, 0, 2));
  return Max(v, SHUFFLE3_M128(v, 2, 0, 1)).X();
}

INLINE Float3 Cross(Float3 a, Float3 b) {
  // x  <-  a.y*b.z - a.z*b.y
  // y  <-  a.z*b.x - a.x*b.z
  // z  <-  a.x*b.y - a.y*b.x
  // We can save a shuffle by grouping it in this wacky order:
  return (a.ZXY()*b - a*b.ZXY()).ZXY();
}

// Returns a 3-bit code where bit0..bit2 is X..Z
INLINE unsigned Mask(Float3 v) { return _mm_movemask_ps(v._m128) & 7; }

// Once we have a comparison, we can branch based on its results:
INLINE bool Any(Float3 v) { return Mask(v) != 0; }
INLINE bool All(Float3 v) { return Mask(v) == 7; }

INLINE Float3 Clamp(Float3 t, Float3 a, Float3 b) { return Min(Max(t, a), b); }
INLINE float Sum(Float3 v) { return v.X() + v.Y() + v.Z(); }
INLINE float Dot(Float3 a, Float3 b) { return Sum(a*b); }
INLINE float Length(Float3 v) { return sqrtf(Dot(v, v)); }
INLINE float LengthSq(Float3 v) { return Dot(v, v); }
INLINE Float3 Normalize(Float3 v) { return v * (1.0f / Length(v)); }
INLINE Float3 Lerp(Float3 a, Float3 b, float t) { return a + (b - a)*t; }

#pragma once

#include "D3D11Dev.h"
#include "Light.h"
#include "PRTEngine.h"
#include "PRTBuilder.h"

class SceneLightingD3D11
{
public:
  struct LightingType
  {
    DirectX::XMFLOAT4 _shLight[3][PRT_BANDS];
  };

  SceneLightingD3D11(D3D11Dev& devD3D11);

  void Apply(ID3D11DeviceContext& devContext);

  size_t AddLight(const Light& light);

  void UpdateLight(size_t idx, const Light& light);

private:
  DirectX::XMFLOAT4 GetData(int bandY, int color);

  PRTEngine _prtEngine;
  std::vector<PRTSampler> _prtSamplers;
  std::vector<Light> _lights;
  std::vector<PRTVector3> _coeffs;
  LightingType _lightingData;
  ComPtr<ID3D11Buffer> _lightingBuffer;
  D3D11Dev& _devD3D11;
};

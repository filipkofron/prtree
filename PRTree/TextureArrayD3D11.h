#pragma once

class TextureArrayD3D11;

#include "TextureD3D11.h"
#include "TextureSamplerD3D11.h"
#include <memory>

class TextureArrayD3D11
{
public:
  TextureArrayD3D11(D3D11Dev& dev);
  void Add(const std::shared_ptr<TextureD3D11>& texture);
  void Clear();
  void BuildResourceView();
  void Serialize(std::ostream& output);
  void Deserialize(std::istream& input);
  const std::vector<std::shared_ptr<TextureD3D11> > GetTextures() const { return _textures; }
  TextureSamplerD3D11& GetSampler() { return _sampler; }
  const TextureSamplerD3D11& GetSampler() const { return _sampler; }
  ID3D11ShaderResourceView** GetTextureResourceView() { return _textureResourceView.GetAddressOf(); }
  size_t Count() const { return _textures.size(); }
protected:
  std::vector<std::shared_ptr<TextureD3D11> > _textures;
  ComPtr<ID3D11ShaderResourceView> _textureResourceView;
  TextureSamplerD3D11 _sampler;
  D3D11Dev& _dev;
};

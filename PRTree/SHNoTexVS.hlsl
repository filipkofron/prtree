#include "SHTex.hlsli"

cbuffer Lighting : register(b2)
{
  float4 shLightR0;
  float4 shLightR1;
  float4 shLightR2;
  float4 shLightR3;

  float4 shLightG0;
  float4 shLightG1;
  float4 shLightG2;
  float4 shLightG3;

  float4 shLightB0;
  float4 shLightB1;
  float4 shLightB2;
  float4 shLightB3;
}

PixelInputType main(VertexInputType input)
{
  PixelInputType output;

  // Change the position vector to be 4 units for proper matrix calculations.
  input.position.w = 1.0f;

  // Calculate the position of the vertex against the world, view, and projection matrices.
  output.position = mul(input.position, worldMatrix);
  output.position = mul(output.position, viewMatrix);
  output.position = mul(output.position, projectionMatrix);

  output.realPos = output.position;
  output.origPos = input.position;
  output.normal = input.normal;

  /*output.shLight = input.shLight0.x + input.shLight0.y + input.shLight0.z + input.shLight0.w;
  output.shLight += input.shLight1.x + input.shLight1.y + input.shLight1.z + input.shLight1.w;
  output.shLight += input.shLight2.x + input.shLight2.y + input.shLight2.z + input.shLight2.w;
  output.shLight += input.shLight3.x + input.shLight3.y + input.shLight3.z + input.shLight3.w;*/


  float4 tempColor = shLightR0 * input.shLight0 + shLightR1 * input.shLight1 + shLightR2 * input.shLight2 + shLightR3 * input.shLight3;
  output.shLight.r = tempColor.x + tempColor.y + tempColor.z + tempColor.w;
  tempColor = shLightG0 * input.shLight0 + shLightG1 * input.shLight1 + shLightG2 * input.shLight2 + shLightG3 * input.shLight3;
  output.shLight.g = tempColor.x + tempColor.y + tempColor.z + tempColor.w;
  tempColor = shLightB0 * input.shLight0 + shLightB1 * input.shLight1 + shLightB2 * input.shLight2 + shLightB3 * input.shLight3;
  output.shLight.b = tempColor.x + tempColor.y + tempColor.z + tempColor.w;

  // todo rest colors


  output.shLight = saturate(output.shLight) * 2.0;


  /*output.shLight = input.shLight0.xyz;
  output.shLight += input.shLight1.xyz;
  output.shLight += input.shLight2.xyz;
  output.shLight += input.shLight3.xyz;
  output.shLight *= 0.01;*/
  // output.normal = float4(0.0, 0.0, 1.0, 0.0);

  // Store the texture coordinates for the pixel shader.
  output.tex = input.tex;

  output.material = input.material;

  return output;
}

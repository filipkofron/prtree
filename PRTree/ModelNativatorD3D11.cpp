#include "ModelNativatorD3D11.h"
#include "ShapeD3D11.h"
#include "MaterialLibraryD3D11.h"
#include "System.h"
#include "UVGeneratorSlow.h"
#include "PRTBuilder.h"
#include <memory>
#include <fstream>
#include <algorithm>

ModelNativatorD3D11::ModelNativatorD3D11(D3D11Dev& devD3D11)
  : _devD3D11(devD3D11)
{
}

void Float4ToXMFloat3(DirectX::XMFLOAT4& dxFl, const float* fl)
{
  dxFl.x = fl[0];
  dxFl.y = fl[1];
  dxFl.z = fl[2];
  dxFl.w = 0.0f;
}

void Float4ToXMFloat4(DirectX::XMFLOAT4& dxFl, const float* fl)
{
  dxFl.x = fl[0];
  dxFl.y = fl[1];
  dxFl.z = fl[2];
  dxFl.w = fl[3];
}

void XMFloat3ToFloat3(float* fl, const DirectX::XMFLOAT3& dxFl)
{
  fl[0] = dxFl.x;
  fl[1] = dxFl.y;
  fl[2] = dxFl.z;
}

struct Float3
{
  Float3(const DirectX::XMFLOAT3& dxFl)
  {
    XMFloat3ToFloat3(_data, dxFl);
  }
  float _data[3];
};

void ModelNativatorD3D11::ConvertToNative(ModelData& modelData)
{
  modelData.GetShapes().clear();
  std::string folder = GSystem->GetFileSystem()->GetDirectoryPath(modelData.GetSource());
  auto matLib = ConvertMaterials(folder, modelData.GetRawMaterials());
  modelData.GetMaterialLibrary() = std::static_pointer_cast<MaterialLibrary>(matLib);
  for (const auto& shape : modelData.GetRawShapes())
  {
    auto convertedShape = ConvertShape(folder, shape, modelData.GetRawMaterials());
    convertedShape->_materialLibrary = matLib;
    modelData.GetShapes().push_back(std::static_pointer_cast<Shape>(convertedShape));
  }
}

static std::string GetFileName(const std::string& source)
{
  return "native_models/" + source + ".bin";
}

bool ModelNativatorD3D11::DeserializeNative(ModelData& modelData)
{
  std::string fileName = GetFileName(modelData.GetSource());
  std::ifstream file(fileName.c_str(), std::ifstream::binary);
  if (file.fail())
    return false;

  auto matLib = std::make_shared<MaterialLibraryD3D11>(_devD3D11);
  if (!matLib->DeserializeNative(file))
    return false;

  modelData.GetMaterialLibrary() = matLib;

  size_t shapeNum;
  file.read(reinterpret_cast<char*>(&shapeNum), sizeof(shapeNum));

  for (size_t s = 0; s < shapeNum; s++)
  {
    auto shape = std::make_shared<ShapeD3D11>(_devD3D11);
    if (!shape->DeserializeNative(file))
      return false;
    shape->_materialLibrary = matLib;
    modelData.GetShapes().push_back(shape);
  }

  return true;
}

void ModelNativatorD3D11::SerializeNative(const ModelData& modelData)
{
  std::string fileName = GetFileName(modelData.GetSource());
  GSystem->GetFileSystem()->CreateDirectoryPath(fileName);
  std::ofstream file(fileName.c_str(), std::ifstream::binary);
  if (file.fail())
    ExitWith("Could not open file " << fileName << " for writing!");

  modelData.GetMaterialLibrary()->SerializeNative(file);

  size_t shapeNum = modelData.GetShapes().size();
  file.write(reinterpret_cast<const char*>(&shapeNum), sizeof(shapeNum));

  for (const auto& shape : modelData.GetShapes())
    shape->SerializeNative(file);

  file.close();
}

static std::vector<bool> FindLeaves(const std::vector<tinyobj::material_t>& materials)
{
  std::vector<bool> result;
  for (size_t i = 0; i < materials.size(); i++)
  {
    const tinyobj::material_t& mat = materials[i];

    // Find leave materials
    std::string leafIndices[] = { "leaf", "leaves" };
    result.push_back(false);
    for (const auto& leafInd : leafIndices)
    {
      std::string lower = mat.name;
      std::transform(lower.begin(), lower.end(), lower.begin(), ::tolower);
      if (lower.find(leafInd) != std::string::npos)
      {
        result[i] = true;
        break;
      }
    }
  }
  return result;
}

std::shared_ptr<ShapeD3D11> ModelNativatorD3D11::ConvertShape(const std::string& folder, const tinyobj::shape_t& shape, const std::vector<tinyobj::material_t>& materials)
{
  auto vertexNum = shape.mesh.positions.size() / 3;
  auto normalNum = shape.mesh.normals.size() / 3;
  auto uvNum = shape.mesh.texcoords.size() / 2;
  auto matNum = shape.mesh.material_ids.size();
  auto indexNum = shape.mesh.indices.size();

  auto nativeShape = new ShapeD3D11(_devD3D11);
  auto tempVertices = new ShapeD3D11::VertexType[vertexNum];
  auto tempIndices = new unsigned long[indexNum];

  std::vector<float> verticesForUV;
  std::vector<int> indicesForUV;

  for (size_t i = 0; i < vertexNum; i++)
  {
    const float* pos = &shape.mesh.positions[i * 3];
    tempVertices[i]._position = { pos[0], pos[1], pos[2] };

    verticesForUV.push_back(pos[0]);
    verticesForUV.push_back(pos[1]);
    verticesForUV.push_back(pos[2]);
    
    if (i < normalNum)
    {
      const float* norm = &shape.mesh.normals[i * 3];
      tempVertices[i]._normal = { norm[0], norm[1], norm[2] };
    }
    else
    {
      tempVertices[i]._normal = { 1, 0, 1 };
    }

    if (i < uvNum)
    {
      const float* uv = &shape.mesh.texcoords[i * 2];
      tempVertices[i]._uv = { uv[0], uv[1] };
    }
    else
    {
      tempVertices[i]._uv = { 0.5, 0.5 };
    }
    /*
    if ((i / 3) < matNum)
    {
      tempVertices[i]._materialId = shape.mesh.material_ids[i / 3];
    }
    else
    {
      tempVertices[i]._materialId = 0;
    }*/
  }

  for (size_t i = 0; i < indexNum; i++)
  {
    tempVertices[shape.mesh.indices[i]]._materialId = shape.mesh.material_ids[i / 3];
  }

  for (size_t i = 0; i < indexNum; i++)
  {
    tempIndices[i] = shape.mesh.indices[i];
    indicesForUV.push_back(tempIndices[i]);
  }

  for (size_t i = 0; i < indexNum; i++)
  {
    tempIndices[i] = shape.mesh.indices[i];
    indicesForUV.push_back(tempIndices[i]);
  }

  // UVGeneratorSlow uvGen;
  // std::vector<float> uvs = uvGen.Generate(verticesForUV, indicesForUV);


  // Build the PRT
  PRTBuilder prtBuilder;
  for (size_t i = 0; i < vertexNum; i++)
  {
    prtBuilder.AddVertex(Float3(tempVertices[i]._position)._data);
    prtBuilder.AddNormal(Float3(tempVertices[i]._normal)._data);
    prtBuilder.AddMatIndex(tempVertices[i]._materialId);
    //prtBuilder.AddMatIndex(0); // temp hack, no albedo
  }

  /*float tempAlbedo[3] = { 1.0f, 1.0f, 1.0f };
  prtBuilder.AddAlbedo(tempAlbedo);*/

  for (size_t i = 0; i < materials.size(); i++)
  {
    prtBuilder.AddAlbedo(materials[i].diffuse);
  }

  for (size_t i = 0; i < materials.size(); i++)
  {
    prtBuilder.AddTransparency(materials[i].dissolve);
  }

  std::vector<bool> leaves = FindLeaves(materials);
  for (size_t i = 0; i < leaves.size(); i++)
  {
    prtBuilder.AddLeaf(leaves[i]);
  }

  for (size_t i = 0; i < indexNum / 3; i++)
  {
    int faceIndices[3] = { tempIndices[i * 3], tempIndices[i * 3 + 1], tempIndices[i * 3 + 2] };
    prtBuilder.AddIndex(faceIndices);
  }

  std::vector<float> prtCoeffs;
  prtBuilder.Build(prtCoeffs);
  
  for (size_t i = 0; i < (prtCoeffs.size() / (4 * 4)); i++)
  {
    Float4ToXMFloat4(tempVertices[i]._shLight0, &prtCoeffs[i * 4 * 4]);
    Float4ToXMFloat4(tempVertices[i]._shLight1, &prtCoeffs[i * 4 * 4 + 4]);
    Float4ToXMFloat4(tempVertices[i]._shLight2, &prtCoeffs[i * 4 * 4 + 8]);
    Float4ToXMFloat4(tempVertices[i]._shLight3, &prtCoeffs[i * 4 * 4 + 12]);
  }

  D3D11_BUFFER_DESC vertexBufferDesc, indexBufferDesc;
  D3D11_SUBRESOURCE_DATA vertexData, indexData;
  HRESULT result;

  vertexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
  vertexBufferDesc.ByteWidth = static_cast<UINT>(sizeof(ShapeD3D11::VertexType) * vertexNum);
  vertexBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
  vertexBufferDesc.CPUAccessFlags = 0;
  vertexBufferDesc.MiscFlags = 0;
  vertexBufferDesc.StructureByteStride = 0;

  // Give the subresource structure a pointer to the vertex data.
  vertexData.pSysMem = tempVertices;
  vertexData.SysMemPitch = 0;
  vertexData.SysMemSlicePitch = 0;

  // Now create the vertex buffer.
  result = _devD3D11.GetDevice().CreateBuffer(&vertexBufferDesc, &vertexData, nativeShape->_vertexBuffer.GetAddressOf());
  if (FAILED(result))
    ExitWith("Could not create vertex buffer!");

  // Set up the description of the static index buffer.
  indexBufferDesc.Usage = D3D11_USAGE_DEFAULT;
  indexBufferDesc.ByteWidth = static_cast<UINT>(sizeof(unsigned long) * indexNum);
  indexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
  indexBufferDesc.CPUAccessFlags = 0;
  indexBufferDesc.MiscFlags = 0;
  indexBufferDesc.StructureByteStride = 0;

  // Give the subresource structure a pointer to the index data.
  indexData.pSysMem = tempIndices;
  indexData.SysMemPitch = 0;
  indexData.SysMemSlicePitch = 0;

  // Create the index buffer.
  result = _devD3D11.GetDevice().CreateBuffer(&indexBufferDesc, &indexData, nativeShape->_indexBuffer.GetAddressOf());
  if (FAILED(result))
    ExitWith("Could not create index buffer!");
  
  nativeShape->_vertexCount = vertexNum;
  nativeShape->_indexCount = indexNum;

  return std::shared_ptr<ShapeD3D11>(nativeShape);
}

std::shared_ptr<MaterialLibraryD3D11> ModelNativatorD3D11::ConvertMaterials(const std::string& folder, const std::vector<tinyobj::material_t>& materials)
{
  auto materialNum = materials.size();

  auto nativeMatLib = new MaterialLibraryD3D11(_devD3D11);
  auto tempMaterial = new MaterialLibraryD3D11::MaterialType;

  std::vector<bool> leaves = FindLeaves(materials);

  for (size_t i = 0; i < materialNum; i++)
  {
    const tinyobj::material_t& mat = materials[i];
    MaterialLibraryD3D11::MaterialType& matType = *tempMaterial;

    Float4ToXMFloat3(matType._ambient[i], mat.ambient);
    nativeMatLib->GetTextureArrayForType(MaterialLibraryD3D11::TextureType::Ambient).Add(std::make_shared<TextureD3D11>(_devD3D11, folder + mat.ambient_texname));
    nativeMatLib->GetTextureArrayForType(MaterialLibraryD3D11::TextureType::Bump).Add(std::make_shared<TextureD3D11>(_devD3D11, folder + mat.bump_texname));
    nativeMatLib->GetTextureArrayForType(MaterialLibraryD3D11::TextureType::Diffuse).Add(std::make_shared<TextureD3D11>(_devD3D11, folder + mat.diffuse_texname));
    nativeMatLib->GetTextureArrayForType(MaterialLibraryD3D11::TextureType::Displacement).Add(std::make_shared<TextureD3D11>(_devD3D11, folder + mat.displacement_texname));
    nativeMatLib->GetTextureArrayForType(MaterialLibraryD3D11::TextureType::SpecularHighlights).Add(std::make_shared<TextureD3D11>(_devD3D11, folder + mat.specular_highlight_texname));
    nativeMatLib->GetTextureArrayForType(MaterialLibraryD3D11::TextureType::Specular).Add(std::make_shared<TextureD3D11>(_devD3D11, folder + mat.specular_texname));
    nativeMatLib->GetTextureArrayForType(MaterialLibraryD3D11::TextureType::Alpha).Add(std::make_shared<TextureD3D11>(_devD3D11, folder + mat.alpha_texname));

    for (int i = 0; i < (int) MaterialLibraryD3D11::TextureType::NTextureTypes; i++)
      nativeMatLib->GetTextureArrayForType((MaterialLibraryD3D11::TextureType) i).BuildResourceView();

    Float4ToXMFloat3(matType._diffuse[i], mat.diffuse);
    matType._sdad[i].x = mat.shininess;
    matType._sdad[i].y = mat.dissolve;
    Float4ToXMFloat3(matType._emission[i], mat.emission);
    Float4ToXMFloat3(matType._specular[i], mat.specular);
    Float4ToXMFloat3(matType._transmittance[i], mat.transmittance);

    matType._flags->x = leaves[i] ? 1.0f : -1.0f;
  }

  D3D11_SUBRESOURCE_DATA materialData;
  HRESULT result;

  D3D11_BUFFER_DESC materialBufferDesc;
  materialBufferDesc.ByteWidth = sizeof(MaterialLibraryD3D11::MaterialType);
  materialBufferDesc.Usage = D3D11_USAGE_DYNAMIC;
  materialBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
  materialBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
  materialBufferDesc.MiscFlags = 0;
  materialBufferDesc.StructureByteStride = 0;

  // Give the subresource structure a pointer to the index data.
  materialData.pSysMem = tempMaterial;
  materialData.SysMemPitch = 0;
  materialData.SysMemSlicePitch = 0;

  // Create the index buffer.
  result = _devD3D11.GetDevice().CreateBuffer(&materialBufferDesc, &materialData, nativeMatLib->_materialBuffer.GetAddressOf());
  if (FAILED(result))
    ExitWith("Could not create material buffer!");

  nativeMatLib->_materialCount = materialNum;
  return std::shared_ptr<MaterialLibraryD3D11>(nativeMatLib);
}

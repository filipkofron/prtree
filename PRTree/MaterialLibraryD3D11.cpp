#include "MaterialLibraryD3D11.h"
#include "System.h"

MaterialLibraryD3D11::MaterialLibraryD3D11(D3D11Dev& dev)
  : _d3d11Dev(dev)
{
  for (int i = 0; i < (int) TextureType::NTextureTypes; i++)
  {
    _textureArrays[i] = std::make_shared<TextureArrayD3D11>(dev);
  }
}

MaterialLibraryD3D11::~MaterialLibraryD3D11()
{
}

bool MaterialLibraryD3D11::DeserializeNative(std::istream& input)
{
  D3D11_BUFFER_DESC matBufferDesc;
  D3D11_SUBRESOURCE_DATA matData;
  HRESULT result;

  size_t matBufferBytes = 0;
  input.read(reinterpret_cast<char*>(&matBufferBytes), sizeof(matBufferBytes));

  if (input.fail())
    ExitWith("Fail while reading native model!");

  uint8_t* pureData = new uint8_t[matBufferBytes];
  input.read(reinterpret_cast<char*>(pureData), matBufferBytes);

  if (input.fail())
    ExitWith("Fail while reading native model!");

  MaterialType* data = reinterpret_cast<MaterialType*>(pureData);

  matBufferDesc.Usage = D3D11_USAGE_DEFAULT;
  matBufferDesc.ByteWidth = static_cast<UINT>(matBufferBytes);
  matBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
  matBufferDesc.CPUAccessFlags = 0;
  matBufferDesc.MiscFlags = 0;
  matBufferDesc.StructureByteStride = 0;

  // Give the subresource structure a pointer to the vertex data.
  matData.pSysMem = pureData;
  matData.SysMemPitch = 0;
  matData.SysMemSlicePitch = 0;

  // Now create the vertex buffer.
  result = _d3d11Dev.GetDevice().CreateBuffer(&matBufferDesc, &matData, _materialBuffer.GetAddressOf());
  if (FAILED(result))
    ExitWith("Could not create vertex buffer!");

  delete[] pureData;

  _materialCount = matBufferBytes / sizeof(MaterialType);

  for (int i = 0; i < (int)TextureType::NTextureTypes; i++)
  {
    _textureArrays[i]->Deserialize(input);
  }

  return true;
}

void MaterialLibraryD3D11::SerializeNative(std::ostream& output)
{
  _d3d11Dev.SerializeBuffer(output, *_materialBuffer.Get());
  for (int i = 0; i < (int)TextureType::NTextureTypes; i++)
  {
    _textureArrays[i]->Serialize(output);
  }
}

void MaterialLibraryD3D11::Apply(ID3D11DeviceContext& devContext)
{
  for (int i = 0; i < (int)TextureType::NTextureTypes; i++)
  {
    _textureArrays[i]->GetSampler().Apply(devContext); // TODO: Only one sampler for now :/
    devContext.PSSetShaderResources(i, 1, _textureArrays[i]->GetTextureResourceView());
  }
}

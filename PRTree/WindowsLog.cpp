#include <windows.h>
#include <stdio.h>
#include <fcntl.h>
#include <io.h>
#include <iostream>
#include <fstream>
#include <memory>
#include <string>

#include "WindowsCommon.h"
#include <debugapi.h>
#include "Log.h"
#include "WindowsLog.h"

#ifndef _USE_OLD_IOSTREAMS
using namespace std;
#endif

// maximum mumber of lines the output console should have
static const WORD MAX_CONSOLE_LINES = 500;

void RedirectIOToConsole()
{
  // allocate a console for this app
  AllocConsole();
  FILE* result = stdout;
  freopen_s(&result, "CONOUT$", "w", stdout);
  // if (!result) TODO: handle result
  ios::sync_with_stdio();
}

class DefaultLogReceiver : public LogReceiver
{
public:
  virtual void OnMessage(const std::string& msg, const LogType& type)
  {
    OutputDebugString(msg.c_str());
    fprintf(stdout, msg.c_str());
    fflush(stdout);
    cout.flush();
  }
};

std::shared_ptr<LogReceiver> CreateDefaultLogReceiver()
{
  return std::make_shared<DefaultLogReceiver>();
}

#include "TextureManagerD3D11.h"
#include "TextureD3D11.h"

std::shared_ptr<TextureD3D11> TextureManagerD3D11::GetTexture(const std::string& path)
{
  if (_cachedTextures.find(path) != _cachedTextures.end())
    return _cachedTextures[path];

  _cachedTextures[path] = std::make_shared<TextureD3D11>(_d3d11Dev, path);

  return _cachedTextures[path];
}

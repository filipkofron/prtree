#pragma once

class System;

#include "Graphics.h"
#include "Input.h"
#include "FileSystem.h"
#include "PreciseTimer.h"
#include "Lang.h"
#include "Log.h"
#include <stdint.h>
#include <string>
#include <ostream>

//! Contains basic system interface
class System
{
public:
  System();
  virtual ~System();

  INLINE bool Initialized() const { return _initialized; }
  void Initialize(bool noGPU);
  void Shutdown();
  virtual void Run() = 0;

  INLINE Graphics* GetGraphics() { return _graphics; }
  INLINE Input* GetInput() { return _input; }
  INLINE FileSystem* GetFileSystem() { return _fileSystem; }
  INLINE const PreciseTimer* GetFrameTimer() { return _frameTimer; }

  virtual float GetTimeFloat() = 0;
  float GetFrameTime();

  bool IsNoGPU() const { return _noGPU; }

  virtual void debug_Sleep(uint64_t millis) = 0;
  virtual std::shared_ptr<LogReceiver> CreateDefaultLogReceiver() = 0;
  virtual void ExitWithError(std::function<void(std::ostream&)> msgFun) = 0;
private:
  System(const System& system) { }

protected:
  std::string _appName;
  bool _initialized;
  bool _noGPU;

  FileSystem* _fileSystem;
  Graphics* _graphics;
  Input* _input;
  PreciseTimer* _frameTimer;
  float _lastFrameTime;

  virtual void OnInit() = 0;
  virtual void OnShutdown() = 0;

  virtual Graphics* CreateGraphics() = 0;
  virtual Input* CreateInput() = 0;
  virtual FileSystem* CreateFileSystem() = 0;
  virtual PreciseTimer* CreatePreciseTimer() = 0;

  bool Frame();

  uint64_t _frame;
};

extern System* GSystem;

#define ExitWith(body) \
  GSystem->ExitWithError([&](std::ostream& os) { os << body; });

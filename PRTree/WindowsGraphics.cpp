#include "WindowsGraphics.h"
#include "Config.h"
#include "SHLightingShaderD3D11.h"
#include "ModelLoaderObj.h"
#include "ModelNativatorD3D11.h"

/* NOT WORKING */
/*
XX("BMW27GE/BMW27GE.obj") \
*/

#define TEST_MODELS(XX) \
  XX("Tree1/Tree1.obj") \
  XX("KrabiceKoule/KrabiceKoule.obj") \
  XX("4Box/4Box.obj") \
  XX("MBox/MBox.obj") \
  XX("simple_tree/simple_tree.obj") \
  XX("TreeV9/Tree_V9_Final.obj") \
  XX("Green_Honey_Myrtle_obj/OC57_9.obj") \
  XX("lod_packed_tree/lod_packed_tree.obj") \
  XX("Rose_flower/Rose_flower.obj") \
  XX("Donut/Donut.obj") \
  XX("Monkey/Monkey.obj") \
  XX("OpenBox/OpenBox.obj") \
  XX("OpenBoxBox/OpenBoxBox.obj") \
  XX("2Planes/2Planes.obj") \
  XX("3Spheres/3Spheres.obj") \
  XX("BrokenPlane/BrokenPlane.obj") \


#define SHADERS(XX) \
  XX("SHTex") \
  XX("SHNoTex") \
  XX("SHSolid") \


#define ARRAY_REGISTER(ITEM_NAME) \
  ITEM_NAME,

#define REGISTER_ARRAY_ITEMS(arrayRegister, ITEMS) \
  arrayRegister = { \
  ITEMS(ARRAY_REGISTER) \
  }

WindowsGraphics::WindowsGraphics(WindowsSystem* system)
  : _system(system),
  _d3d11Dev(system->GetWindowHandle()),
  _sceneLighting(_d3d11Dev),
  _currentModel(5),
 // _currentModel(13),
  _currentShader(-1),
  _modelRotatation(true),
  _timeLastModelRot(0.0f),
  _timeLast(0.0f)
{
  REGISTER_ARRAY_ITEMS(_registeredModels, TEST_MODELS);
  REGISTER_ARRAY_ITEMS(_registeredShaders, SHADERS);

  _camera.SetPosition(0.232893, -0.141763, 3.28175);
  _camera.SetRotation(0.00631643, 3.28404, 0);

  SwitchModel(true);
  SwitchShader();

  Float3 dir(1.0, -1, 1.0);
  Light sun(Light::Type::Directional, Float3(0,0,0), Normalize(dir), Float3(0, 0, 0));

  _sceneLighting.AddLight(sun);
  _sceneLighting.AddLight(sun); //temp hack
  _sceneLighting.AddLight(sun); //temp hack
  _sceneLighting.AddLight(sun);// will be overwritten
}

WindowsGraphics::~WindowsGraphics()
{
}

void WindowsGraphics::Render()
{
  const float zNear = 0.1f;
  const float zFar = 10000.0f;

  float fieldOfView = (float) PI_D / 4.0f;
  float screenAspect = (float) CurrentConfig._width / (float) CurrentConfig._height;

  _d3d11Dev.InitMatrices(fieldOfView, screenAspect, zNear, zFar);
  //_d3d11Dev.BeginScene(0.15f, 0.1f, 0.13f, 0.5f);
  _d3d11Dev.BeginScene(0.3f, 0.3f, 0.4, 0.0f);

  float objRotTimeCoef = 0.001f;

  float time = GSystem->GetTimeFloat();
  float timeDelta = time - _timeLast;
  if (!_modelRotatation)
  {
    timeDelta = 0.0f;
  }
  _timeLast = time;
  _timeLastModelRot += timeDelta;

  for (int i = 0; i < 1; i++)
  {
    DirectX::XMMATRIX worldMatrix = DirectX::XMMatrixIdentity();
    worldMatrix = DirectX::XMMatrixTranslation(i, 0, 0) * worldMatrix;

    worldMatrix = DirectX::XMMatrixRotationY(_timeLastModelRot * objRotTimeCoef) * worldMatrix;
    _d3d11Dev.SetWorldMatrix(worldMatrix);

    _camera.Update();
    _shader->Apply(_d3d11Dev);
    _d3d11Dev.ApplyMatrices(_camera);
    _sceneLighting.Apply(_d3d11Dev.GetDeviceContext());

    if (_model)
      _model->Render(_d3d11Dev.GetDeviceContext());
  }


  _d3d11Dev.EndScene();
  

//  float timeCoef = 0.001f;
  float timeCoef = -0.001f;

  float time0 = GSystem->GetTimeFloat() * timeCoef;
  float time1 = GSystem->GetTimeFloat() * timeCoef + PI_F * 2.0 / 3.0;
  float time2 = GSystem->GetTimeFloat() * timeCoef + PI_F * 4.0 / 3.0;
 
  
  /*
  float sunIntensity = 3.0f;
  Float3 sunColor = Float3(1.0, 0.0, 0.0) * sunIntensity;
  Float3 dir(sin(time0), -0.3, cos(time0));
  Light sun(Light::Type::Directional, Float3(0, 0, 0), Normalize(dir), sunColor);
  _sceneLighting.UpdateLight(0, sun);

  Float3 sun1Color = Float3(0.0, 1.0, 0.0) * sunIntensity;
  Float3 dir1(sin(time1), -0.3, cos(time1));
  Light sun1(Light::Type::Directional, Float3(0, 0, 0), Normalize(dir1), sun1Color);
  _sceneLighting.UpdateLight(1, sun1);

  Float3 sun2Color = Float3(0.0, 0.0, 1.0) * sunIntensity;
  Float3 dir2(sin(time2), -0.3, cos(time2));
  Light sun2(Light::Type::Directional, Float3(0, 0, 0), Normalize(dir2), sun2Color);
  _sceneLighting.UpdateLight(2, sun2);*/



  float sunIntensity = 3.0f;
  Float3 sunColor = Float3(1.0, 0.96, 0.92) * sunIntensity;
  Float3 dir(sin(time0), 0.5, cos(time0));
  Light sun(Light::Type::Directional, Float3(0, 0, 0), Normalize(dir), sunColor);
  _sceneLighting.UpdateLight(0, sun);



  float skyIntensity = 2.0f;
  Float3 skyColor = Float3(0.8, 0.9, 1.0) * skyIntensity;
  Float3 skyDir(0, 1, 0);
  Light sky(Light::Type::HalfSphere, Float3(0, 0, 0), Normalize(skyDir), skyColor);
  _sceneLighting.UpdateLight(3, sky);

  // temporary to quickly shutdown
  if (GSystem->IsNoGPU())
  {
    exit(0);
  }
}

Camera* WindowsGraphics::GetCamera()
{
  return &_camera;
}

void WindowsGraphics::SwitchModel(bool forward)
{
  if (_registeredModels.size() == 0)
  {
    Error << "No model available!" << "\n";
    return;
  }

  if (forward)
  {
    _currentModel = (_currentModel + 1) % _registeredModels.size();
  }
  else
  {
    if (_currentModel == 0 || _currentModel == -1)
    {
      _currentModel = _registeredModels.size();
    }
    _currentModel = (_currentModel - 1) % _registeredModels.size();
  }
  _model = std::make_unique<ModelD3D11>(std::make_shared<ModelLoaderObj>(std::make_shared<ModelNativatorD3D11>(_d3d11Dev)), _registeredModels[_currentModel], _d3d11Dev.GetDevice());
}

void WindowsGraphics::SwitchShader()
{
  if (_registeredShaders.size() == 0)
  {
    Error << "No shader available!" << "\n";
    return;
  }

  _currentShader = (_currentShader + 1) % _registeredShaders.size();
  _shader = std::make_unique<SHLightingShaderD3D11>(_d3d11Dev.GetDevice(), _registeredShaders[_currentShader]);
}

void WindowsGraphics::SwitchModelRotation()
{
  _modelRotatation = !_modelRotatation;
}

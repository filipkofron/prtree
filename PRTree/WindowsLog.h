#pragma once

#include "WindowsCommon.h"
#include "Log.h"
#include <memory>
std::shared_ptr<LogReceiver> CreateDefaultLogReceiver();
void RedirectIOToConsole();

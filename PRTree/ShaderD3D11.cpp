#include "ShaderD3D11.h"
#include "System.h"
#include "WindowsCommon.h"
#include "StringUtils.h"
#include <string>

ShaderD3D11::ShaderD3D11(ID3D11Device& dev, const char* name)
  : _layoutSetup(false), _name(name)
{
  std::string vsName = name;
  vsName = GetExecutableDir() + "/" + vsName + "VS.cso";
  std::string psName = name;
  psName = GetExecutableDir() + "/" + psName + "PS.cso";

  std::wstring fileName = StringUtils::MultibyteStringToWide(vsName.c_str());
  if (FAILED(D3DReadFileToBlob(fileName.data(), &_vertexShaderBuffer)))
    ExitWith("Could not load vertex shader file: " << vsName);
  if (FAILED(dev.CreateVertexShader(_vertexShaderBuffer->GetBufferPointer(), _vertexShaderBuffer->GetBufferSize(), NULL, &_vertexShader)))
    ExitWith("Could not create vertex shader: " << vsName);

  fileName = StringUtils::MultibyteStringToWide(psName.c_str());
  if (FAILED(D3DReadFileToBlob(fileName.data(), &_pixelShaderBuffer)))
    ExitWith("Could not load pixel shader file: " << psName);
  if (FAILED(dev.CreatePixelShader(_pixelShaderBuffer->GetBufferPointer(), _pixelShaderBuffer->GetBufferSize(), NULL, &_pixelShader)))
    ExitWith("Could not create pixel shader: " << psName);

  Debug << "[Shader] Loaded " << name << " shader." << "\n";
}

ShaderD3D11::~ShaderD3D11()
{
}

void ShaderD3D11::Apply(D3D11Dev& dev)
{
  ID3D11Device& d3dDev = dev.GetDevice();
  ID3D11DeviceContext& d3dDevContext = dev.GetDeviceContext();
  if (!_layoutSetup)
  {
    SetupLayout(d3dDev);
    _layoutSetup = true;
  }
  // Set the vertex input layout.
  d3dDevContext.IASetInputLayout(_layout.Get());

  // Set the vertex and pixel shaders that will be used to render this triangle.
  d3dDevContext.VSSetShader(_vertexShader.Get(), NULL, 0);
  d3dDevContext.PSSetShader(_pixelShader.Get(), NULL, 0);

  OnApply(dev);
}

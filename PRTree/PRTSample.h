#pragma once

#include "PRTVector.h"
#include <vector>

struct PRTSample
{
  PRTVector2 _sphereCoord;
  PRTVector3 _coord;
  std::vector<float> _shFunctions;
};

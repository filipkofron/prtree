#pragma once

#ifdef _MSC_VER
#define INLINE __forceinline
#else // _MSC_VER
#define INLINE attribute((always_inline))
#endif // _MSC_VER

#ifdef DEBUG
undef DEBUG
#endif
#define DEBUG _DEBUG

#define PI_D    (3.14159265358979323846)
#define PI_F    (3.14159265358979323846f)
/*
void AssertPars(bool val, int lineNum, const char* filename, const char* expr);

//#define Assert(expr) AssertPars(expr, __LINE__, $@, #expr)

#if DEBUG
#define DebugAssert(expr) AssertPars(expr)
#else
#define DebugAssert(expr)
#endif*/

float RandomFloatUnit();
int RandomInt();
void InitRandom();

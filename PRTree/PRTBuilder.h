#pragma once

#include "PRTModel.h"
#define PRT_BANDS 4

static void AddF3(std::vector<PRTVector3>& dst, const float* src)
{
  dst.push_back({ src[0], src[1], src[2] });
}

class PRTBuilder
{
private:
  PRTModel _model;

public:
  void AddVertex       (const float* f3) { AddF3(_model._vertices, f3); }
  void AddNormal       (const float* f3) { AddF3(_model._normals, f3); }
  void AddIndex        (const int*   i3) { _model._vertexIndices.push_back({ i3[0], i3[1], i3[2] }); }
  void AddMatIndex     (int          i ) { _model._materialIndices.push_back(i); }
  void AddAlbedo       (const float* f3) { AddF3(_model._albedo, f3); }
  void AddTransparency (float        f ) { _model._transparency.push_back(f); }
  void AddLeaf         (bool         b ) { _model._leaf.push_back(b); }


  void Build(std::vector<float>& coeffs);
};

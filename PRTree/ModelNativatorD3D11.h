#pragma once

class ModelNativatorD3D11;

#include "D3D11Dev.h"
#include "ModelNativator.h"
#include "ShapeD3D11.h"
#include "MaterialLibraryD3D11.h"

class ModelNativatorD3D11 : public ModelNativator
{
public:
  ModelNativatorD3D11(D3D11Dev& devD3D11);
  virtual void ConvertToNative(ModelData& modelData) override;
  virtual bool DeserializeNative(ModelData& modelData) override;
  virtual void SerializeNative(const ModelData& modelData) override;
private:
  D3D11Dev& _devD3D11;
  std::shared_ptr<ShapeD3D11> ConvertShape(const std::string& folder, const tinyobj::shape_t& shape, const std::vector<tinyobj::material_t>& materials);
  std::shared_ptr<MaterialLibraryD3D11> ConvertMaterials(const std::string& folder, const std::vector<tinyobj::material_t>& materials);
};

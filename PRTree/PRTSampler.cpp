#include "PRTSampler.h"
#include "Lang.h"
#include "System.h"

void PRTSampler::ComputeSamples(int squareN)
{
  for (int i = 0; i < squareN; i++)
    for (int j = 0; j < squareN; j++)
    {
      float a = ((float)i + Random()) / (float)squareN;
      float b = ((float)j + Random()) / (float)squareN;

      float elevation = 2 * acos(sqrt(1.0f - a));
      float azimuth = 2 * PI_F * b;

      float x = sin(elevation) * cos(azimuth);
      float y = sin(elevation) * sin(azimuth);
      float z = cos(elevation);

      int k = i * squareN + j;
      _samples.push_back({ {elevation, azimuth}, {x, y, z} });
    }

  float test = 0.0f;
  for (int i = 0; i < 10000; i++)
  {
    test += Random();
  }

  test /= 10000;
}

float PRTSampler::Random()
{
  return RandomFloatUnit();
}
#pragma once

#include "D3D11Dev.h"

class ShaderD3D11
{
public:
  ShaderD3D11(ID3D11Device& dev, const char* name);
  virtual ~ShaderD3D11();

  void Apply(D3D11Dev& dev);
  const std::string& GetName() const { return _name; }
protected:
  bool _layoutSetup;
  virtual void SetupLayout(ID3D11Device& dev) = 0;
  virtual void OnApply(D3D11Dev& dev) { }

  ComPtr<ID3DBlob> _vertexShaderBuffer;
  ComPtr<ID3DBlob> _pixelShaderBuffer;
  ComPtr<ID3D11VertexShader> _vertexShader;
  ComPtr<ID3D11PixelShader> _pixelShader;
  ComPtr<ID3D11InputLayout> _layout;
  std::string _name;
};

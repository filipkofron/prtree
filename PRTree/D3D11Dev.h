#pragma once

#define D3D_DEBUG_INFO

#pragma comment(lib, "dxgi.lib")
#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "D3DCompiler.lib")

#include <dxgi.h>
#include <d3dcommon.h>
#include <d3d11.h>
#include <WICTextureLoader.h>
#include <D3Dcompiler.h>
#include <DirectXMath.h>
#include <wrl.h>
#include <string>
#include <memory>
#include <vector>

class D3D11Dev;

#include "CameraD3D11.h" // TODO: Remove
#include "TextureManagerD3D11.h"

using namespace Microsoft::WRL;

class D3D11Dev
{
public:
  D3D11Dev(HWND hwnd);
  virtual ~D3D11Dev();

  void BeginScene(float red, float green, float blue, float alpha);
  void EndScene();

  void InitMatrices(float fov, float aspect, float zNear, float zFar);

  ID3D11Device& GetDevice();
  ID3D11DeviceContext& GetDeviceContext();
  TextureManagerD3D11* GetTextureManager() { return _textureManager.get(); }

  void ApplyMatrices(CameraD3D11& camera);

  void GetProjectionMatrix(DirectX::XMMATRIX&);
  void SetWorldMatrix(const DirectX::XMMATRIX& worldMatrix) { _worldMatrix = worldMatrix; }
  void GetOrthoMatrix(DirectX::XMMATRIX&);

  void GetVideoCardInfo(char*, int&);

  void SerializeBuffer(std::ostream& output, ID3D11Buffer& buffer);
  void ReadBufferToRaw(ID3D11Buffer& buffer, std::vector<float>& dest);
  void ReadBufferToRaw(ID3D11Buffer& buffer, std::vector<int>& dest);

private:
  struct MatrixBufferType
  {
    DirectX::XMMATRIX _world;
    DirectX::XMMATRIX _view;
    DirectX::XMMATRIX _projection;
  };

  int _videoCardMemory;
  std::string _videoCardDescription;
  IDXGISwapChain* _swapChain;
  ID3D11Device* _device;
  ID3D11DeviceContext* _deviceContext;
  ID3D11Debug* _deviceDebug;
  ID3D11RenderTargetView* _renderTargetView;
  ID3D11Texture2D* _depthStencilBuffer;
  ID3D11DepthStencilState* _depthStencilState;
  ID3D11DepthStencilView* _depthStencilView;
  ID3D11RasterizerState* _rasterState;
  HWND _hwnd;
  DirectX::XMMATRIX _projectionMatrix;
  DirectX::XMMATRIX _worldMatrix;
  DirectX::XMMATRIX _orthoMatrix;
  ID3D11Buffer* _matrixBuffer;
  std::unique_ptr<TextureManagerD3D11> _textureManager;
};
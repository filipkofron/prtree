#pragma once

#include "UVGenerator.h"

class UVGeneratorSlow : public UVGenerator
{
public:
  virtual std::vector<float> Generate(const std::vector<float>& vertices, const std::vector<int>& indices) override;
};

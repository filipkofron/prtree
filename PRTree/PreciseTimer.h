#pragma once

class PreciseTimer
{
public:
  virtual void Reset() = 0;
  virtual double MeasureNow() const = 0;
};

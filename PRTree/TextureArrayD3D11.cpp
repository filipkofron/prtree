#include "TextureArrayD3D11.h"
#include "System.h"

TextureArrayD3D11::TextureArrayD3D11(D3D11Dev& dev)
  : _sampler(dev), _dev(dev)
{
}

void TextureArrayD3D11::Add(const std::shared_ptr<TextureD3D11>& texture)
{
  _textures.push_back(texture);
}

void TextureArrayD3D11::Clear()
{
  _textures.clear();
}

void TextureArrayD3D11::BuildResourceView()
{
  int minWidth = 512;
  int minHeight = 512;
  int minMipLevels = 16;
  //DXGI_FORMAT texFormat = DXGI_FORMAT_B8G8R8A8_UNORM;
  DXGI_FORMAT texFormat = DXGI_FORMAT_R8G8B8A8_UNORM_SRGB;
  std::vector<ComPtr<ID3D11Texture2D> > cpuTextures;
  cpuTextures.resize(_textures.size());
  for (int i = 0; i < _textures.size(); i++)
  {
    ID3D11Texture2D* tex2D;
    HRESULT hr = _textures[i]->GetTexture()->QueryInterface(IID_ID3D11Texture2D, (void **) &tex2D);
    if (FAILED(hr))
      ExitWith("Invalid texture passed, expected Texture2D!");

    D3D11_TEXTURE2D_DESC sourceDesc;
    tex2D->GetDesc(&sourceDesc);
    if (i == 0 || sourceDesc.Width < minWidth)
      minWidth = sourceDesc.Width;
    if (i == 0 || sourceDesc.Height < minHeight)
      minHeight = sourceDesc.Height;
    if (i == 0 || sourceDesc.MipLevels < minMipLevels)
      minMipLevels = sourceDesc.MipLevels;
  //  texFormat = sourceDesc.Format;

    D3D11_TEXTURE2D_DESC cpuDesc = sourceDesc;
    cpuDesc.Usage = D3D11_USAGE_STAGING;
    cpuDesc.CPUAccessFlags = D3D11_CPU_ACCESS_READ;
    cpuDesc.MiscFlags = 0;
    cpuDesc.BindFlags = 0;
    hr = _dev.GetDevice().CreateTexture2D(&cpuDesc, nullptr, cpuTextures[i].GetAddressOf());
    if (FAILED(hr))
      ExitWith("Fail creating texture 2D!");
    _dev.GetDeviceContext().CopyResource(cpuTextures[i].Get(), _textures[i]->GetTexture());
  }

  D3D11_TEXTURE2D_DESC texArrayDesc;
  texArrayDesc.Width = minWidth;
  texArrayDesc.Height = minHeight;
  texArrayDesc.MipLevels = minMipLevels;
  texArrayDesc.ArraySize = _textures.size();
  texArrayDesc.Format = texFormat;
  texArrayDesc.SampleDesc.Count = 1;
  texArrayDesc.SampleDesc.Quality = 0;
  texArrayDesc.Usage = D3D11_USAGE_DEFAULT;
  texArrayDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
  texArrayDesc.CPUAccessFlags = 0;
  texArrayDesc.MiscFlags = 0;
  ComPtr<ID3D11Texture2D> texArray;
  HRESULT hr = _dev.GetDevice().CreateTexture2D(&texArrayDesc, 0, texArray.GetAddressOf());
  if (FAILED(hr))
    ExitWith("Failed creating texture 2D array!");

  for (int i = 0; i < cpuTextures.size(); i++)
  {
    auto sourceTexture = cpuTextures[i];
    // for each mipmap level...
    for (UINT mipLevel = 0; mipLevel < minMipLevels; ++mipLevel)
    {
      D3D11_MAPPED_SUBRESOURCE mappedTex2D;
      hr = _dev.GetDeviceContext().Map(sourceTexture.Get(), mipLevel, D3D11_MAP_READ, 0, &mappedTex2D);
      if (FAILED(hr))
        ExitWith("Failed mapping the texture mip map level!");
      _dev.GetDeviceContext().UpdateSubresource(texArray.Get(), D3D11CalcSubresource(mipLevel, i, minMipLevels),
        0, mappedTex2D.pData, mappedTex2D.RowPitch, mappedTex2D.DepthPitch);
      _dev.GetDeviceContext().Unmap(sourceTexture.Get(), mipLevel);
    }
  }
  //
  // Create a resource view to the texture array.
  //
  D3D11_SHADER_RESOURCE_VIEW_DESC viewDesc;
  viewDesc.Format = texArrayDesc.Format;
  viewDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2DARRAY;
  viewDesc.Texture2DArray.MostDetailedMip = 0;
  viewDesc.Texture2DArray.MipLevels = texArrayDesc.MipLevels;
  viewDesc.Texture2DArray.FirstArraySlice = 0;
  viewDesc.Texture2DArray.ArraySize = cpuTextures.size();

  _dev.GetDevice().CreateShaderResourceView(texArray.Get(), &viewDesc, _textureResourceView.GetAddressOf());
}

void TextureArrayD3D11::Serialize(std::ostream& output)
{
  size_t size = _textures.size();
  output.write(reinterpret_cast<const char*>(&size), sizeof(size_t));
  for (size_t i = 0; i < size; i++)
  {
    size_t len = _textures[i]->GetSource().size();
    output.write(reinterpret_cast<const char*>(&len), sizeof(size_t));
    output.write(_textures[i]->GetSource().c_str(), _textures[i]->GetSource().size());
  }
}

void TextureArrayD3D11::Deserialize(std::istream& input)
{
  size_t size = 0;
  input.read(reinterpret_cast<char*>(&size), sizeof(size_t));
  char buffer[FILENAME_MAX * 2];
  for (size_t i = 0; i < size; i++)
  {
    size_t len = 0;
    input.read(reinterpret_cast<char*>(&len), sizeof(size_t));
    input.read(buffer, len);
    buffer[len] = '\0';
    Add(std::make_shared<TextureD3D11>(_dev, buffer));
  }
  BuildResourceView();
}

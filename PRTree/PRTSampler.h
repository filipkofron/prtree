#pragma once

#include "PRTSample.h"
#include <vector>

class PRTSampler
{
public:
  void ComputeSamples(int squareN);
  const std::vector<PRTSample>& GetSamples() const { return _samples; }
  std::vector<PRTSample>& GetSamples() { return _samples; }
protected:
  float Random();
  std::vector<PRTSample> _samples;
};
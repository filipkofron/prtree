#pragma once

class WindowsFileSystem;

#include "FileSystem.h"

class WindowsFileSystem : public FileSystem
{
public:
  virtual void CreateDirectoryPath(const std::string& path) override;
  virtual std::string GetWorkingDirectory() override;
  virtual std::string GetDirectoryPath(const std::string& path) override;
};
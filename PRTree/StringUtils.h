#pragma once

#include "System.h"
#include <string>

namespace StringUtils
{
  std::string WideStringToMultibyte(const std::wstring& wideStr);
  std::wstring MultibyteStringToWide(const std::string& wideStr);
}

#include "TextureSamplerD3D11.h"

TextureSamplerD3D11::TextureSamplerD3D11(D3D11Dev& dev)
{
  D3D11_SAMPLER_DESC desc;
  desc.Filter = D3D11_FILTER_ANISOTROPIC;
  desc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
  desc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
  desc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
  desc.MipLODBias = 0;
  desc.MaxAnisotropy = 4;
  desc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
  desc.BorderColor[0] = 1;
  desc.BorderColor[1] = 1;
  desc.BorderColor[2] = 1;
  desc.BorderColor[3] = 1;
  desc.MinLOD = 0;
  desc.MaxLOD = 1024;
  dev.GetDevice().CreateSamplerState(&desc, _sampler.GetAddressOf());
}

void TextureSamplerD3D11::Apply(ID3D11DeviceContext& devContext)
{
  devContext.PSSetSamplers(0, 1, _sampler.GetAddressOf());
}

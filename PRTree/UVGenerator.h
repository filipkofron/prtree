#pragma once

#include "Shape.h"

class UVGenerator
{
public:
  virtual std::vector<float> Generate(const std::vector<float>& vertices, const std::vector<int>& indices) = 0;
};

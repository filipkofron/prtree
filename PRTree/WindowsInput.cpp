#include "WindowsInput.h"

#pragma comment(lib, "dinput8.lib")
#pragma comment(lib, "dxguid.lib")

WindowsInput::WindowsInput(WindowsSystem* system)
  : _system(system)
{
  memset(_keyPressed, 0, sizeof(_keyPressed));
}

void WindowsInput::OnWinKeyDown(unsigned int key)
{
  //Debug << key << " was pressed down" << "\n";
  CheckKey(key);
  if (!_keyPressed[key])
    OnKeyPressDown(key);
  _keyPressed[key] = true;
}

void WindowsInput::OnWinKeyUp(unsigned int key)
{
  //Debug << key << " was pressed up" << "\n";
  CheckKey(key);
  if (_keyPressed[key])
    OnKeyPressed(key);
  _keyPressed[key] = false;
}

void WindowsInput::OnKeyPressed(unsigned int key)
{
  // key press (single click, then repeat)
}

void WindowsInput::OnKeyPressDown(unsigned int key)
{
  switch (key)
  {
  case VK_END: // END
    GSystem->GetGraphics()->SwitchModelRotation();
    break;
  case VK_PRIOR: // PAGE UP
    GSystem->GetGraphics()->SwitchModel(false);
    break;
  case VK_NEXT: // PAGE DOWN 
    GSystem->GetGraphics()->SwitchModel(true);
    break;
  case VK_RETURN: // ENTER
    GSystem->GetGraphics()->SwitchShader();
    break;
  }
}

void WindowsInput::HandleKey(unsigned int key)
{
  float speedTranslate = 0.02f * GSystem->GetFrameTime();
  float speedRotate = 0.02f * GSystem->GetFrameTime();
  switch (key)
  {
  case VK_DOWN:
    GSystem->GetGraphics()->GetCamera()->TranslateSmooth(0.0f, 0.0f, -speedTranslate, speedTranslate);
    break;
  case VK_UP:
    GSystem->GetGraphics()->GetCamera()->TranslateSmooth(0.0, 0.0f, speedTranslate, speedTranslate);
    break;
  case VK_CONTROL:
    GSystem->GetGraphics()->GetCamera()->TranslateSmooth(0.0f, -speedTranslate, 0.0f, speedTranslate);
    break;
  case VK_SHIFT:
    GSystem->GetGraphics()->GetCamera()->TranslateSmooth(0.0f, speedTranslate, 0.0f, speedTranslate);
    break;
  case VK_LEFT:
    GSystem->GetGraphics()->GetCamera()->TranslateSmooth(-speedTranslate, 0.0f, 0.0f, speedTranslate);
    break;
  case VK_RIGHT:
    GSystem->GetGraphics()->GetCamera()->TranslateSmooth(speedTranslate, 0.0f, 0.0f, speedTranslate);
    break;
  case VK_NUMPAD6:
    GSystem->GetGraphics()->GetCamera()->RotateSmooth(0.0f, speedRotate, 0.0f, speedRotate);
    break;
  case VK_NUMPAD4:
    GSystem->GetGraphics()->GetCamera()->RotateSmooth(0.0f, -speedRotate, 0.0f, speedRotate);
    break;
  case VK_NUMPAD2:
    GSystem->GetGraphics()->GetCamera()->RotateSmooth(speedRotate, 0.0f, 0.0f, speedRotate);
    break;
  case VK_NUMPAD8:
    GSystem->GetGraphics()->GetCamera()->RotateSmooth(-speedRotate, 0.0f, 0.0f, speedRotate);
    break;
  case VK_ESCAPE:
    GSystem->Shutdown();
    break;
  }
}

bool WindowsInput::ReadMouse()
{
  HRESULT result;

  // Read the mouse device.
  result = _mouse->GetDeviceState(sizeof(DIMOUSESTATE), (LPVOID) &_mouseState);
  if (FAILED(result))
  {
    // If the mouse lost focus or was not acquired then try to get control back.
    if ((result == DIERR_INPUTLOST) || (result == DIERR_NOTACQUIRED))
    {
      _mouse->Acquire();
    }
    else
    {
      return false;
    }
  }

  return true;
}

void WindowsInput::Update()
{
  for (int i = 0; i < KEYS_MAX; i++)
  {
    if (_keyPressed[i])
      HandleKey(i);
  }
}

void WindowsInput::CheckKey(unsigned int key)
{
  if (key >= sizeof(_keyPressed))
    ExitWith("Invalid key received: " << key);
}

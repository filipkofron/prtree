#pragma once

#include "MaterialLibrary.h"
#include "D3D11Dev.h"
#include "TextureArrayD3D11.h"

#define MATERIALS_MAX 32

class MaterialLibraryD3D11 : public MaterialLibrary
{
  friend class ModelNativatorD3D11;
public:
  enum class TextureType : int
  {
    Ambient,
    Diffuse,
    Specular,
    SpecularHighlights,
    Bump,
    Displacement,
    Alpha,

    NTextureTypes
  };

  __declspec(align(16)) struct MaterialType
  {
    DirectX::XMFLOAT4 _ambient[MATERIALS_MAX];
    DirectX::XMFLOAT4 _diffuse[MATERIALS_MAX];
    DirectX::XMFLOAT4 _specular[MATERIALS_MAX];
    DirectX::XMFLOAT4 _transmittance[MATERIALS_MAX];
    DirectX::XMFLOAT4 _emission[MATERIALS_MAX];
    DirectX::XMFLOAT4 _sdad[MATERIALS_MAX]; // [shininess coef, dissolve coef, ambient tex id, diffuse tex id]
    DirectX::XMFLOAT4 _ssbd[MATERIALS_MAX]; // [specular tex id, specural high tex id, bump tex id, displacement tex id]
    DirectX::XMFLOAT4 _flags[MATERIALS_MAX]; // [leaf bool(-1 or 1), transparency, none, none]
  };

  MaterialLibraryD3D11(D3D11Dev& dev);
  virtual ~MaterialLibraryD3D11();

  virtual bool DeserializeNative(std::istream& input) override;
  virtual void SerializeNative(std::ostream& output) override;

  const ComPtr<ID3D11Buffer>& GetMaterialBuffer() const { return _materialBuffer; }
  void Apply(ID3D11DeviceContext& devContext);
  TextureArrayD3D11& GetTextureArrayForType(TextureType type) { return *_textureArrays[(int)type]; }
protected:
  D3D11Dev& _d3d11Dev;
  ComPtr<ID3D11Buffer> _materialBuffer;
  std::shared_ptr<TextureArrayD3D11> _textureArrays[(int) TextureType::NTextureTypes];
  size_t _materialCount;
};

#include "UVGeneratorSlow.h"
#include "System.h"
#include <vector>

struct Vec2
{
  float x, y;
};

struct Vec3
{
  float x, y, z;
};

struct Indexed
{
  int index;
};

struct Face2D
{
  Vec2 a, b, c;
  size_t index;

  void AddToVector(std::vector<float>& vec)
  {
    vec.push_back(a.x); vec.push_back(a.y);
    vec.push_back(b.x); vec.push_back(b.y);
    vec.push_back(c.x); vec.push_back(c.y);
  }
};

INLINE float square(float a)
{
  return a * a;
}

struct Face3D
{
  Vec3 a, b, c;
  size_t index;

  Face2D flatten()
  {
    Face2D res;
    res.a.x = 0;
    res.a.y = 0;

    res.b.x = sqrt(square(b.x - a.x) + square(b.y - a.y) + square(b.z - a.z));
    res.b.y = 0;
      
    res.c.x = ((b.x - a.x) * (c.x - a.x) +
      (b.y - a.y) * (c.y - a.y) +
      (b.z - a.z) * (c.z - a.z)) / res.b.x;
    res.c.y = sqrt(square(c.x - a.x) + square(c.y - a.y) + square(c.z - a.z) - square(res.c.x));

    res.index = index;

    return res;
  }
};

static const float GFaceSidePix = 8;

static float STOF(size_t a)
{
  return static_cast<float>(a);
}

static size_t FTOS(float a)
{
  return static_cast<size_t>(a);
}

class UVAllocator
{
public:
  UVAllocator(size_t facesCount)
    : _currRowFaces(0),
    _currColumnFaces(0)
  {
    _sideSize = FTOS(ceil(static_cast<float>(sqrt(facesCount))));
  }

  void AssignNext(Face2D& face)
  {
    if (_currRowFaces & 1)
    {
      face.a = { STOF(_currRowFaces      ),  STOF(_currColumnFaces      ) };
      face.b = { STOF(_currRowFaces      ),  STOF((_currColumnFaces + 1)) };
      face.c = { STOF((_currRowFaces + 1)),  STOF((_currColumnFaces + 1)) };
    }
    else
    {
      face.a = { STOF(_currRowFaces      ),   STOF(_currColumnFaces    )   };
      face.b = { STOF((_currRowFaces + 1)),   STOF((_currColumnFaces + 1)) };
      face.c = { STOF((_currRowFaces + 1)),   STOF(_currColumnFaces     )  };
    }

    const float sizeInv = 1.0f / _sideSize;
    face.a.x *= sizeInv; face.b.x *= sizeInv; face.c.x *= sizeInv;
    face.a.y *= sizeInv; face.b.y *= sizeInv; face.c.y *= sizeInv;

    _currRowFaces = (_currRowFaces + 1) % _sideSize;
    if (!_currRowFaces)
    {
      _currColumnFaces++;
    }
  }
private:
  size_t _sideSize;
  size_t _currRowFaces;
  size_t _currColumnFaces;
};

std::vector<float> UVGeneratorSlow::Generate(const std::vector<float>& vertices, const std::vector<int>& indices)
{
  Info << "Vertices count: " << vertices.size() << "\n";
  Info << "Indices count: " << indices.size() << "\n";

  std::vector<Face2D> faces;
  for (size_t i = 0; i < indices.size() / 3; i++)
  {
    Face3D f =
    {
      { vertices[indices[i * 3] * 3], vertices[indices[i * 3 + 1] * 3], vertices[indices[i * 3 + 2] * 3] },
      { vertices[indices[i * 3] * 3 + 1], vertices[indices[i * 3 + 1] * 3 + 1], vertices[indices[i * 3 + 2] * 3 + 1] },
      { vertices[indices[i * 3] * 3 + 2], vertices[indices[i * 3 + 1] * 3 + 2], vertices[indices[i * 3 + 2] * 3 + 2] },
      i
    };

    faces.push_back(f.flatten());
  }

  UVAllocator uvAllocator(faces.size());
  // temp hack, do 8 * 8 for 2 faces, ignoring the real size
  for (size_t i = 0; i < faces.size(); i++)
  {
    uvAllocator.AssignNext(faces[i]);
  }
  
  std::vector<float> uvCoordsRaw;
  for (size_t i = 0; i < faces.size(); i++)
  {
    faces[i].AddToVector(uvCoordsRaw);
  }

  return uvCoordsRaw;
}

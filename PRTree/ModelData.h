#pragma once

#include <string>

class ModelData;

#include "ModelLoader.h"
#include "tiny_obj_loader.h"
#include "Shape.h"
#include "MaterialLibrary.h"

class ModelData
{
public:
  ModelData(const std::string& source) : _source(source) { }
  virtual ~ModelData();
  std::vector<std::shared_ptr<Shape> >& GetShapes() { return _shapes; }
  std::shared_ptr<MaterialLibrary>& GetMaterialLibrary() { return _materialLibrary; }
  std::vector<tinyobj::shape_t>& GetRawShapes() { return _rawShapes; }
  std::vector<tinyobj::material_t>& GetRawMaterials() { return _rawMaterials; }
  const std::vector<std::shared_ptr<Shape> >& GetShapes() const { return _shapes; }
  const std::shared_ptr<MaterialLibrary>& GetMaterialLibrary() const { return _materialLibrary; }
  const std::vector<tinyobj::shape_t>& GetRawShapes() const { return _rawShapes; }
  const std::vector<tinyobj::material_t>& GetRawMaterials() const { return _rawMaterials; }
  const std::string& GetSource() const { return _source; }

  void UnloadRaw();
private:
  std::string _source;
  std::vector<tinyobj::shape_t> _rawShapes;
  std::vector<tinyobj::material_t> _rawMaterials;
  std::vector<std::shared_ptr<Shape> > _shapes;
  std::shared_ptr<MaterialLibrary> _materialLibrary;
};

#include "SceneLightingD3D11.h"
#include "System.h"

SceneLightingD3D11::SceneLightingD3D11(D3D11Dev& devD3D11)
  : _devD3D11(devD3D11)
{
  D3D11_BUFFER_DESC lightBufferDesc;
  _prtSamplers.resize(1);
  _prtSamplers[0].ComputeSamples(16); // 16^2

  PRTEngine engine;
  engine.ComputeSH(_prtSamplers, 4);

  lightBufferDesc.ByteWidth = sizeof(LightingType);
  lightBufferDesc.Usage = D3D11_USAGE_DEFAULT;
  lightBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
  lightBufferDesc.CPUAccessFlags = 0; // D3D11_CPU_ACCESS_WRITE;
  lightBufferDesc.MiscFlags = 0;
  lightBufferDesc.StructureByteStride = 0;

  // Create the index buffer.
  HRESULT result = _devD3D11.GetDevice().CreateBuffer(&lightBufferDesc, nullptr, _lightingBuffer.GetAddressOf());
  if (FAILED(result))
    ExitWith("Could not create lighting buffer!");
}

void SceneLightingD3D11::Apply(ID3D11DeviceContext& devContext)
{
  _prtEngine.ProjectLight(_coeffs, _lights, _prtSamplers[0], 4);

  for (int color = 0; color < 3; color++)
  {
    for (int component = 0; component < PRT_BANDS; component++)
    {
      _lightingData._shLight[color][component] = GetData(component, color);
    }
  }
  
  devContext.UpdateSubresource(_lightingBuffer.Get(), 0, 0, &_lightingData, 0, 0);
  devContext.VSSetConstantBuffers(2, 1, _lightingBuffer.GetAddressOf());
}

size_t SceneLightingD3D11::AddLight(const Light& light)
{
  _lights.push_back(light);
  return _lights.size() - 1;
}

void SceneLightingD3D11::UpdateLight(size_t idx, const Light& light)
{
  _lights[idx] = light;
}

inline DirectX::XMFLOAT4 SceneLightingD3D11::GetData(int bandY, int color)
{
  int idx = bandY * 4;
  return DirectX::XMFLOAT4(_coeffs[idx + 0][color], _coeffs[idx + 1][color], _coeffs[idx + 2][color], _coeffs[idx + 3][color]);
}


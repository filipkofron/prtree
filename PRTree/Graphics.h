#pragma once

#include "Camera.h"

class Graphics
{
public:
  virtual void Render() = 0;
  virtual int ScreenWidth() = 0;
  virtual int ScreenHeight() = 0;
  virtual Camera* GetCamera() = 0;
  virtual void SwitchModel(bool forward) = 0;
  virtual void SwitchShader() = 0;
  virtual void SwitchModelRotation() = 0;
};

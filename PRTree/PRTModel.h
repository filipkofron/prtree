#pragma once

#include "PRTVector.h"
#include <vector>

struct PRTModel
{
  std::vector<PRTVector3> _vertices;
  std::vector<PRTVector3> _normals;
  std::vector<int> _materialIndices;
  std::vector<PRTVector3Int> _vertexIndices;
  std::vector<PRTVector3> _albedo;
  std::vector<float> _transparency;
  std::vector<bool> _leaf;

  
};
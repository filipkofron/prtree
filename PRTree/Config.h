#pragma once

#define CONFIG_PARAMS(DECL_INT, DECL_BOOL) \
  DECL_BOOL(fullscreen, false) \
  DECL_BOOL(vsync, false) \
  DECL_INT(width, 1920) \
  DECL_INT(height, 1080)

#define CONFIG_DECL_DEFAULT_CONSTR(name, defVal) _##name(defVal),
#define CONFIG_DECL_PARAMS_INT(name, defVal) int _##name;
#define CONFIG_DECL_PARAMS_BOOL(name, defVal) bool _##name;

struct Config
{
  Config() : CONFIG_PARAMS(CONFIG_DECL_DEFAULT_CONSTR, CONFIG_DECL_DEFAULT_CONSTR) _dummy(0) { }
  CONFIG_PARAMS(CONFIG_DECL_PARAMS_INT, CONFIG_DECL_PARAMS_BOOL)

private:
  int _dummy;
};

extern Config CurrentConfig;
////////////////////////////////////////////////////////////////////////////////
// Filename: main.cpp
////////////////////////////////////////////////////////////////////////////////
#include "Version.h"
#include "System.h"
#include "WindowsSystem.h"
#include "Lang.h"
#include "Log.h"
#include <iostream>

System* GSystem;

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR pScmdline, int iCmdshow)
{
  Info << PRTREE_PROGRAM_NAME << "\n";
  InitRandom();
  GSystem = new WindowsSystem;
  auto logReceiver = GSystem->CreateDefaultLogReceiver();
  Logger::AddLogReceiver(logReceiver);

  bool noGPU = false;
  std::string cmd = pScmdline;
  if (cmd.find("nogpu") != std::string::npos)
  {
    noGPU = true;
  }

  GSystem->Initialize(noGPU);

  if (GSystem->Initialized())
    GSystem->Run();

  GSystem->Shutdown();
  Logger::RemoveLogReceiver(logReceiver);
  delete GSystem;
  GSystem = nullptr;

  return 0;
}

#include "Model.h"

Model::Model(const std::shared_ptr<ModelLoader>& loader, const std::string& source)
  : _data(loader->LoadModel(source))
{

}

Model::~Model()
{

}

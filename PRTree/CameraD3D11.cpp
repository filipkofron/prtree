#include "CameraD3D11.h"
#include "System.h"

CameraD3D11::CameraD3D11()
  : _position(DirectX::XMFLOAT3(0.0f, 0.0f, 0.0f)),
  _rotation(DirectX::XMFLOAT3(0.0f, 0.0f, 0.0f)),
  _viewMatrix(DirectX::XMMatrixIdentity())
{
}

CameraD3D11::~CameraD3D11()
{
}

void CameraD3D11::SetPosition(float x, float y, float z)
{
  _position.x = x;
  _position.y = y;
  _position.z = z;
}

void CameraD3D11::SetRotation(float x, float y, float z)
{
  _rotation.x = x;
  _rotation.y = y;
  _rotation.z = z;
}

void CameraD3D11::Update()
{
  UpdateSmoothMovement();

  DirectX::XMFLOAT3 upF3, lookAtF3;
  DirectX::XMVECTOR up, lookAt, position;
  DirectX::XMMATRIX rotationMatrix;

  // Setup the vector that points upwards.
  upF3.x = 0.0f;
  upF3.y = 1.0f;
  upF3.z = 0.0f;

  // Setup where the camera is looking by default.
  lookAtF3.x = 0.0f;
  lookAtF3.y = 0.0f;
  lookAtF3.z = 1.0f;

  // Create the rotation matrix from the yaw, pitch, and roll values.
  rotationMatrix = DirectX::XMMatrixRotationRollPitchYaw(_rotation.x, _rotation.y, _rotation.z);

  lookAt = DirectX::XMLoadFloat3(&lookAtF3);
  // Transform the lookAt and up vector by the rotation matrix so the view is correctly rotated at the origin.
  lookAt = DirectX::XMVector3TransformCoord(lookAt, rotationMatrix);

  up = DirectX::XMLoadFloat3(&upF3);
  up = DirectX::XMVector3TransformCoord(up, rotationMatrix);

  position = DirectX::XMLoadFloat3(&_position);
  // Translate the rotated camera position to the location of the viewer.
  lookAt = DirectX::XMVectorAdd(position, lookAt);

  // Finally create the view matrix from the three updated vectors.
  _viewMatrix = DirectX::XMMatrixLookAtLH(position, lookAt, up);

 //  Info << _position.x << ", " << _position.y << ", " << _position.z << "\n";
 //  Info << _rotation.x << ", " << _rotation.y << ", " << _rotation.z << "\n";
}

void CameraD3D11::UpdateSmoothMovement()
{
  for (auto i = _changesWanted.begin(); i != _changesWanted.end(); ++i)
  {
    float diff = GSystem->GetTimeFloat() - i->_timeStarted;
    float time = i->_time * 1000.0f;
    float diffToEnd = diff - time;
    float distance = min(diff, diffToEnd) / time;
    float diffLast = diff - i->_timeDone;
    float coefToDo = diffLast / time * distance * distance;

    _rotation.x += i->_rotation.x * coefToDo;
    _rotation.y += i->_rotation.y * coefToDo;
    _rotation.z += i->_rotation.z * coefToDo;

    _position.x += i->_position.x * coefToDo;
    _position.y += i->_position.y * coefToDo;
    _position.z += i->_position.z * coefToDo;

    i->_timeDone += diffLast;

    if (time <= diff)
    {
      i = _changesWanted.erase(i);
      if (i == _changesWanted.end())
        break;
      continue;
    }
  }
}

DirectX::XMMATRIX CameraD3D11::GetViewMatrix()
{
  return _viewMatrix;
}

void CameraD3D11::Translate(float x, float y, float z)
{
  _position.x += x;
  _position.y += y;
  _position.z += z;
}

void CameraD3D11::Rotate(float x, float y, float z)
{
  _rotation.x += x;
  _rotation.y += y;
  _rotation.z += z;
}

void CameraD3D11::TranslateSmooth(float x, float y, float z, float time)
{
  ChangeWanted changeWanted;
  changeWanted._position.x = x;
  changeWanted._position.y = y;
  changeWanted._position.z = z;
  changeWanted._time = time;
  _changesWanted.push_back(changeWanted);
}

void CameraD3D11::RotateSmooth(float x, float y, float z, float time)
{
  ChangeWanted changeWanted;
  changeWanted._rotation.x = x;
  changeWanted._rotation.y = y;
  changeWanted._rotation.z = z;
  changeWanted._time = time;
  _changesWanted.push_back(changeWanted);
}

CameraD3D11::ChangeWanted::ChangeWanted()
  : _time(0), _timeStarted(GSystem->GetTimeFloat()), _timeDone(0)
{
  _position = { 0.0f, 0.0f, 0.0f };
  _rotation = { 0.0f, 0.0f, 0.0f };
}

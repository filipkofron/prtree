#pragma once

class WindowsGraphics;

#include "WindowsCommon.h"
#include "Graphics.h"
#include "WindowsSystem.h"
#include "D3D11Dev.h"
#include "ModelD3D11.h"
#include "ShaderD3D11.h"
#include "TextureD3D11.h"
#include "SceneLightingD3D11.h"
#include "Config.h"

class WindowsGraphics : public Graphics
{
public:
  WindowsGraphics(WindowsSystem* system);
  virtual ~WindowsGraphics();

  virtual void Render() override;
  virtual int ScreenWidth() override { return CurrentConfig._width; }
  virtual int ScreenHeight() override { return CurrentConfig._height; }

  virtual Camera* GetCamera() override;
  virtual void SwitchModel(bool forward) override;
  virtual void SwitchShader() override;
  virtual void SwitchModelRotation() override;

private:
  WindowsSystem* _system;
  D3D11Dev _d3d11Dev;
  std::unique_ptr<ModelD3D11> _model;
  SceneLightingD3D11 _sceneLighting;
  std::unique_ptr<ShaderD3D11> _shader;
  CameraD3D11 _camera;
  bool _modelRotatation;
  float _timeLastModelRot;
  float _timeLast;

  std::vector<std::string> _registeredModels;
  int _currentModel = 0;
  std::vector<std::string> _registeredShaders;
  int _currentShader = 0;
};

#include "PRTEngine.h"
#include "Lang.h"
#include "PRTVector.h"
#include "System.h"
#include "BVH.h"
#include <algorithm>
#include <cmath>

float PRTEngine::Legendre(int l, int m, float x)
{
  float res;
  if (l == m + 1)
    res = x * (2 * m + 1) * Legendre(m, m, x);
  else if (l == m)
    res = powf(-1, m) * DoubleFactorial(2 * m - 1) * powf((1 - x * x), m / 2);
  else
    res = (x * (2 * l - 1) * Legendre(l - 1, m, x) - (l + m - 1) * Legendre(l - 2, m, x)) / (l - m);

  return res;
}

float PRTEngine::DoubleFactorial(int n)
{
  if (n <= 1)
    return 1;
  else
    return n * DoubleFactorial(n - 2);
}

INLINE int factorial(int x, int result = 1)
{
  if (x <= 1)
  {
    return result;
  }
  else
  {
    return factorial(x - 1, x * result);
  }
}

float PRTEngine::Normalize(int l, int m)
{
  float num = (2 * l + 1) * factorial(l - abs(m));
  float denom = 4 * PI_F * factorial(l + abs(m));
  return sqrt(num / denom);
}

float PRTEngine::SphericalHarmonic(int l, int m, float elevation, float azimuth)
{
  float res;
  if (m > 0)
    res = sqrtf(2) * Normalize(l, m) * cos(m * azimuth) * Legendre(l, m, cos(elevation));
  else if (m < 0)
    res = sqrtf(2) * Normalize(l, m) * sin(-m * azimuth) * Legendre(l, -m, cos(elevation));
  else
    res = Normalize(l, m) * Legendre(l, 0, cos(elevation));

  return res;
}

void PRTEngine::ComputeSH(std::vector<PRTSampler>& samplers, int bands)
{
  for (auto& sampler : samplers)
  {
    for (auto& sample : sampler.GetSamples())
    {
      sample._shFunctions.resize(bands * bands);
      for (int l = 0; l < bands; l++)
        for (int m = -l; m <= l; m++)
        {
          int j = l * (l + 1) + m;
          sample._shFunctions[j] = SphericalHarmonic(l, m, sample._sphereCoord._x, sample._sphereCoord._y);
        }
    }
  }
}

static void cross(float* res, float* a, float* b)
{
  res[0] = a[1] * b[2] - a[2] * b[1];
  res[1] = a[2] * b[0] - a[0] * b[2];
  res[2] = a[0] * b[1] - a[1] * b[0];
}

static void cross(float* res, const PRTVector3& a, float* b)
{
  res[0] = a._y * b[2] - a._z * b[1];
  res[1] = a._z * b[0] - a._x * b[2];
  res[2] = a._x * b[1] - a._y * b[0];
}

static float dot(float* a, float* b)
{
  return a[0] * b[0] + a[1] * b[1] + a[2] * b[2];
}

static float dot(const PRTVector3& a, float* b)
{
  return a._x * b[0] + a._y * b[1] + a._z * b[2];
}

static float dot(const PRTVector3& a, const PRTVector3& b)
{
  return a._x * b._x + a._y * b._y + a._z * b._z;
}

#define kEpsilon 1e-6
// Culling would cause tree leaves to darken from one side as they have no volume
//#define CULLING

bool RayTriangleIntersect(
  const Float3 &orig, const Float3 &dir,
  const Float3 &v0, const Float3 &v1, const Float3 &v2,
  float &t, float &u, float &v)
{
  Float3 v0v1 = v1 - v0;
  Float3 v0v2 = v2 - v0;
  Float3 pvec = Cross(dir, v0v2);
  float det = Dot(v0v1, pvec);
#ifdef CULLING 
  // if the determinant is negative the triangle is backfacing
  // if the determinant is close to 0, the ray misses the triangle
  if (det < kEpsilon) return false;
#else 
  // ray and triangle are parallel if det is close to 0
  if (fabs(det) < kEpsilon) return false;
#endif 
  float invDet = 1 / det;

  Float3 tvec = orig - v0;
  u = Dot(tvec, pvec) * invDet;
  if (u < 0 || u > 1) return false;

  Float3 qvec = Cross(tvec, v0v1);
  v = Dot(dir, qvec) * invDet;
  if (v < 0 || u + v > 1) return false;

  t = Dot(v0v2, qvec) * invDet;

  if (t < kEpsilon)
    return false;

  return true;
}

bool PRTEngine::RayIntersectTriangle(const PRTVector3& pos, const PRTVector3& dir, const PRTVector3& v0, const PRTVector3& v1, const PRTVector3& v2)
{
  float e1[3] = { (v1._x - v0._x), (v1._y - v0._y), (v1._z - v0._z) };
  float e2[3] = { (v2._x - v0._x), (v2._y - v0._y), (v2._z - v0._z) };
  float h[3];
  cross(h, dir, e2);
  float a = dot(e1, h);
  if (a > -0.00001f && a < 0.00001f)
    return(false);
  float f = 1.0f / a;
  float s[3] = { pos._x - v0._x, pos._y - v0._y, pos._z - v0._z };
  float u = f * dot(s, h);
  if (u < 0.0f || u > 1.0f)
    return(false);
  float q[3];
  cross(q, s, e1);
  float v = f * dot(dir, q);
  if (v < 0.0f || u + v > 1.0f)
    return(false);
  float t = dot(e2, q)*f;
  if (t < 0.00001f)
    return(false);
  return(true);
}

bool PRTEngine::Visible(const PRTModel& model, int vertex, const PRTVector3& dir)
{
  bool visible = true;
  const PRTVector3& p = model._vertices[vertex];
  for (int i = 0; i < model._vertexIndices.size(); i++)
  {
    const PRTVector3Int& triangle = model._vertexIndices[i];
    if ((vertex != triangle._x) && (vertex != triangle._y) && (vertex != triangle._z))
    {
      const PRTVector3& v0 = model._vertices[triangle._x];
      const PRTVector3& v1 = model._vertices[triangle._y];
      const PRTVector3& v2 = model._vertices[triangle._z];
      visible = !RayIntersectTriangle(p, dir, v0, v1, v2);
      //float t, u, v;
     // visible = !::RayTriangleIntersect(p, dir, v0, v1, v2, t, u, v);
      if (!visible)
        break;
    }
  }
  return visible;
}

struct BVHTriangle : public BVHObject
{
private:
  Float3 _a;
  Float3 _b;
  Float3 _c;
  int        _matId;
public:

  BVHTriangle(Float3 a, Float3 b, Float3 c, int matId)
    : _a(a), _b(b), _c(c), _matId(matId)
  {
  }

  //! All "Objects" must be able to test for intersections with rays.
  virtual bool getIntersection(
    const Ray& ray,
    IntersectionInfo* intersection)
    const
  {
    float t, u, v;
    Float3 orig(ray.o.m128);
    Float3 dir(ray.d.m128);

    float eps = 0.001f; // we might need to add the eps, but values should reallz be the same unless they are modified throught the BVH
    // check if the point is not the triangle itself
   // if (_a.Compare(orig, eps) || _b.Compare(orig, eps) || _c.Compare(orig, eps))
    if (LengthSq(_a - orig) < eps || LengthSq(_b - orig) < eps || LengthSq(_c - orig) < eps)
      return false;

    if (::RayTriangleIntersect(orig, dir, _a, _b, _c, t, u, v))
    {
      intersection->object = this;
      Vector3& hit = intersection->hit;
      hit = (orig + dir * t).M128();

      intersection->t = t;
      return true;
    }
    return false;
  }

  //! Return an object normal based on an intersection
  virtual Vector3 getNormal(const IntersectionInfo& I) const
  {
    Float3 u = _b - _a;
    Float3 v = _c - _a;
    Float3 cross = Cross(u, v);

    return Vector3(cross.M128());
  }

  //! Return a bounding box for this object
  virtual BBox getBBox() const
  {
    Float3 min = Min(_a, Min(_b, _c));
    Float3 max = Max(_a, Max(_b, _c));

    float eps = 0.001;

    return BBox(Vector3(min.X() - eps, min.Y() - eps, min.Z() - eps), Vector3(max.X() + eps, max.Y() + eps, max.Z() + eps));
  }

  //! Return the centroid for this object. (Used in BVH Sorting)
  virtual Vector3 getCentroid() const
  {
    Float3 avg =( _a + _b + _c) * (1.0f / 3.0f);
    return Vector3(avg.M128());
  }

  int GetMaterialID() const { return _matId; }
};


Float3 PRT3ToFlt3(const PRTVector3& vec)
{
  return Float3(vec._x, vec._y, vec._z);
}

void PRTEngine::ProjectShadowed(std::vector<std::vector<PRTVector3> >& coeffs, std::vector<PRTSampler>& samplers, const PRTModel& model, int bands)
{
  coeffs.resize(model._vertices.size());
  for (int i = 0; i < model._vertices.size(); i++)
  {
    coeffs[i].resize(bands * bands);
    for (int j = 0; j < bands*bands; j++)
    {
      coeffs[i][j]._x = 0.0f;
      coeffs[i][j]._y = 0.0f;
      coeffs[i][j]._z = 0.0f;
    }
  }

  std::vector<BVHObject*> bvhTriangles;
  for (int i = 0; i < model._vertexIndices.size(); i++)
  {
    PRTVector3Int v = model._vertexIndices[i];
    bvhTriangles.push_back(new BVHTriangle(PRT3ToFlt3(model._vertices[v._x]), PRT3ToFlt3(model._vertices[v._y]), PRT3ToFlt3(model._vertices[v._z]), model._materialIndices[v._x]));
  }

  int workerCnt = std::thread::hardware_concurrency();
  Info << "Detected " << workerCnt << " cores.\n";
  if (workerCnt == 0)
    workerCnt = 1;

  if (workerCnt > 1)
    workerCnt--;
  //workerCnt = 1; // TEMP
  Info << "Using " << workerCnt << " cores.\n";

  size_t workPerWorker = model._vertices.size() / workerCnt;
  workPerWorker += model._vertices.size() % workerCnt;

  BVH** bvh = new BVH*[workerCnt];
  for (int i = 0; i < workerCnt; i++)
  {
    bvh[i] = new BVH(&bvhTriangles, 16);
  }
  std::vector<std::thread> workers;
  for (size_t w = 0; w < workerCnt; w++)
  {
    workers.push_back(std::thread([&](size_t workerId)
    {
      Info << "Starting thread  " << workerId << "\n";
      for (size_t i = workerId; i < model._vertices.size(); i += workerCnt)
      {
        if (workerId == 0 && (i % (10 * workerCnt)) == workerCnt)
          Info << "Sampling vertex " << i << "\n";

        int randomSampler = abs(RandomInt()) % samplers.size();
        const PRTSampler& sampler = samplers[randomSampler];
        float scale = 1.0f / sampler.GetSamples().size();
        for (const auto& sample : sampler.GetSamples())
        {
          const PRTVector3& p = model._vertices[i];
          const PRTVector3& dir = sample._coord;
          Ray ray(Vector3(p._x, p._y, p._z), Vector3(dir._x, dir._y, dir._z));
          IntersectionInfo info;

          bool cont;
          float overAllVisibility = 1.0f;
          do
          {
            bool intersect = bvh[workerId]->getIntersection(ray, &info, false);
            cont = intersect;
            //bool intersect = !Visible(model, i, sample._coord);

            if (intersect)
            {
              // we know for sure the instance is simly inherted BVHTriangle
              const BVHTriangle* hit = static_cast<const BVHTriangle*>(info.object);
              float invTransparency = model._transparency[hit->GetMaterialID()];
              if (invTransparency > 1.0 - kEpsilon)
              {
                cont = false;
                overAllVisibility = 0.0f;
              }
              else
              {
                overAllVisibility *= 1.0f - invTransparency;
                float coef = 100.0f * kEpsilon;
                Vector3 off(ray.d.x * coef, ray.d.y * coef, ray.d.z * coef);
                ray.o = info.hit + off;
              }
            }
          } while (cont);

          /*if (bvh[workerId]->getIntersection(ray, &info, false))
          {
            overAllVisibility = 0.0f;
          }*/

          if (overAllVisibility > kEpsilon)
          {
            int material = model._materialIndices[i];
            float cosine_term;
            if (model._leaf[material])
            {
              cosine_term = fabs(dot(model._normals[i], sample._coord));
            }
            else
            {
              cosine_term = dot(model._normals[i], sample._coord);
            }

            for (int k = 0; k < bands * bands; k++)
            {
              float sh_function = sample._shFunctions[k] * scale;
              //PRTVector3 albedo = model._albedo[material].Mul(overAllVisibility);
              PRTVector3 albedo = { overAllVisibility, overAllVisibility, overAllVisibility };
              coeffs[i][k]._x += (albedo._x * sh_function * cosine_term);
              coeffs[i][k]._y += (albedo._y * sh_function * cosine_term);
              coeffs[i][k]._z += (albedo._z * sh_function * cosine_term);
              /*for (int k = 0; k < bands * bands; k++)
              {
              coeffs[i][k]._x += 1.0f / (bands * bands * sampler.GetSamples().size());
              coeffs[i][k]._y += 1.0f / (bands * bands * sampler.GetSamples().size());
              coeffs[i][k]._z += 1.0f / (bands * bands * sampler.GetSamples().size());
              }*/

            }
          }
        }

        /*for (int k = 0; k < bands * bands; k++)
        {
          float y = abs(model._vertices[i]._y - 0.5);
          coeffs[i][k]._x = 1.0f / (bands * bands) * y * 0.5;
          coeffs[i][k]._y = 1.0f / (bands * bands) * y * 0.5;
          coeffs[i][k]._z = 1.0f / (bands * bands) * y * 0.5;
        }*/
      }
    }, w));
  }
  std::for_each(workers.begin(), workers.end(), [](std::thread &t)
  {
    t.join();
  });
  for (int i = 0; i < workerCnt; i++)
  {
    delete bvh[i];
  }
  delete[] bvh;
  for (int i = 0; i < model._vertexIndices.size(); i++)
  {
    delete bvhTriangles[i];
  }
}

void PRTEngine::ProjectLight(std::vector<PRTVector3>& coeffs, std::vector<Light> lights, PRTSampler& sampler, int bands)
{
  coeffs.resize(bands * bands);
  for (int i = 0; i < bands * bands; i++)
  {
    coeffs[i]._x = 0.0f;
    coeffs[i]._y = 0.0f;
    coeffs[i]._z = 0.0f;
  }

  for (int i = 0; i < bands * bands; i++)
  {
    for (int l = 0; l < lights.size(); l++)
    {
      for (const auto& sample : sampler.GetSamples())
      {
        float sh_function = sample._shFunctions[i];
        Float3 dir = Float3(sample._coord._x, sample._coord._y, sample._coord._z);
        Float3 color = lights[l].EvalAtDir(dir);
        coeffs[i]._x += (color.X() * sh_function);
        coeffs[i]._y += (color.Y() * sh_function);
        coeffs[i]._z += (color.Z() * sh_function);
      }
    }
  }

  float weight = 4.0f * PI_F;
  float scale = weight / sampler.GetSamples().size();
  for (int i = 0; i < bands*bands; i++)
  {
    coeffs[i]._x *= scale;
    coeffs[i]._y *= scale;
    coeffs[i]._z *= scale;
  }
}

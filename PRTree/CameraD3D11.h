#pragma once

class CameraD3D11;

#include "Camera.h"
#include "D3D11Dev.h"
#include <vector>

class CameraD3D11 : public Camera
{
public:
  CameraD3D11();
  ~CameraD3D11();

  void SetPosition(float x, float y, float z);
  void SetRotation(float x, float y, float z);

  DirectX::XMFLOAT3 GetPosition();
  DirectX::XMFLOAT3 GetRotation();

  void Update();
  void UpdateSmoothMovement();
  DirectX::XMMATRIX GetViewMatrix();

  virtual void Translate(float x, float y, float z) override;
  virtual void Rotate(float x, float y, float z) override;
  virtual void TranslateSmooth(float x, float y, float z, float time) override;
  virtual void RotateSmooth(float x, float y, float z, float time) override;

private:
  DirectX::XMFLOAT3 _position;
  DirectX::XMFLOAT3 _rotation;
  DirectX::XMMATRIX _viewMatrix;

  struct ChangeWanted
  {
    ChangeWanted();
    DirectX::XMFLOAT3 _position;
    DirectX::XMFLOAT3 _rotation;
    float _time;
    float _timeDone;
    float _timeStarted;
  };
  std::vector<ChangeWanted> _changesWanted;
};
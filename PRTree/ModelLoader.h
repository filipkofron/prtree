#pragma once

#include <memory>
#include <map>

class ModelLoader;

#include "ModelData.h"

class ModelLoader
{
protected:
  std::map<std::string, std::shared_ptr<ModelData> > _cachedModels;
public:
  virtual std::shared_ptr<ModelData> LoadModel(const std::string& source) = 0;
};

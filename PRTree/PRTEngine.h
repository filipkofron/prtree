#pragma once

#include "PRTSampler.h"
#include "PRTModel.h"
#include "Light.h"

class PRTEngine
{
public:
  float Legendre(int l, int m, float x);
  float DoubleFactorial(int n);
  float Normalize(int l, int m);
  float SphericalHarmonic(int l, int m, float elevation, float azimuth);
  void ComputeSH(std::vector<PRTSampler>& samplers, int bands);

  bool RayIntersectTriangle(const PRTVector3& pos, const PRTVector3& dir, const PRTVector3& v0, const PRTVector3& v1, const PRTVector3& v2);
  bool Visible(const PRTModel& model, int vertex, const PRTVector3& dir);
  void ProjectShadowed(std::vector<std::vector<PRTVector3> >& coeffs, std::vector<PRTSampler>& samplers, const PRTModel& model, int bands);
  void ProjectLight(std::vector<PRTVector3>& coeffs, std::vector<Light> lights, PRTSampler & sampler, int bands);
};
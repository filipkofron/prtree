#pragma once

#include "ShaderD3D11.h"

class SHLightingShaderD3D11 : public ShaderD3D11
{
public:
  SHLightingShaderD3D11(ID3D11Device& dev, const std::string& name);
  virtual ~SHLightingShaderD3D11();

protected:
  virtual void SetupLayout(ID3D11Device& dev) override;
  virtual void OnApply(D3D11Dev& dev) override;

  ComPtr<ID3D11SamplerState> _samplerState;
  ComPtr<ID3D11BlendState> _blendState;
};

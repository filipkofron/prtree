#pragma once

class ShapeD3D11;

#include "Shape.h"
#include "D3D11Dev.h"
#include "MaterialLibraryD3D11.h"

class ShapeD3D11 : public Shape
{
  friend class ModelNativatorD3D11;

public:
  ShapeD3D11(D3D11Dev& dev);
  virtual ~ShapeD3D11();

  void Render(ID3D11DeviceContext& dev);

  virtual bool DeserializeNative(std::istream& input) override;
  virtual void SerializeNative(std::ostream& output) override;

  virtual size_t GetVerticesCount() override;
  virtual void GetVertices(std::vector<float>& dest) override;

  virtual size_t GetIndexesCount() override;
  virtual void GetIndexes(std::vector<int>& dest) override;

  struct VertexType
  {
    DirectX::XMFLOAT3 _position;
    DirectX::XMFLOAT3 _normal;
    DirectX::XMFLOAT2 _uv;
    UINT _materialId;
    //DirectX::XMFLOAT2 _shapeUv;
    DirectX::XMFLOAT4 _shLight0;
    DirectX::XMFLOAT4 _shLight1;
    DirectX::XMFLOAT4 _shLight2;
    DirectX::XMFLOAT4 _shLight3;
  };

private:
  void RenderBuffers(ID3D11DeviceContext& devContext);

  ComPtr<ID3D11Buffer> _vertexBuffer;
  ComPtr<ID3D11Buffer> _indexBuffer;
  std::shared_ptr<MaterialLibraryD3D11> _materialLibrary;
  size_t _vertexCount;
  size_t _indexCount;
  D3D11Dev& _d3d11Dev;
};

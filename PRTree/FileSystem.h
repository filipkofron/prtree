#pragma once

#include <string>

class FileSystem
{
public:
  virtual void CreateDirectoryPath(const std::string& path) = 0;
  virtual std::string GetWorkingDirectory() = 0;
  virtual std::string GetDirectoryPath(const std::string& path) = 0;
};
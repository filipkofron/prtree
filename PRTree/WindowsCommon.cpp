#include "WindowsCommon.h"
#include "System.h"
#include "StringUtils.h"

std::string GetExecutableDir()
{
  HMODULE hModule = GetModuleHandleW(NULL);
  WCHAR wpath[MAX_PATH + 1];
  GetModuleFileNameW(hModule, wpath, MAX_PATH);
  std::string res = StringUtils::WideStringToMultibyte(wpath);
  return res.substr(0, res.find_last_of("\\/"));
}
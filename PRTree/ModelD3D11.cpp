#include "ModelD3D11.h"
#include "System.h"

ModelD3D11::ModelD3D11(const std::shared_ptr<ModelLoader>& loader, const std::string& source, ID3D11Device& dev)
  : Model(loader, source)
{
}

ModelD3D11::~ModelD3D11()
{
}

void ModelD3D11::Render(ID3D11DeviceContext& devContext)
{
  for (auto& shape : _data->GetShapes())
  {
    Shape* shapePtr = shape.get();
    ShapeD3D11* shapeD3D11 = dynamic_cast<ShapeD3D11*>(shapePtr);
    shapeD3D11->Render(devContext);
  }
}

#pragma once

class Input
{
public:
  virtual void Update() = 0;
};
